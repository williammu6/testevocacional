//
//  APIServer.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 29/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import Alamofire

class APIServer{
    
    
    class func requestToken(closure: @escaping (String)->Void){
        print("Req Tok")
        var token : String = ""
        var type : String = ""
        let parameters : [String:String] = ["client_id": "vocacional", "client_secret": "8HVl=YnO", "grant_type":"client_credentials"]
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        //.authenticate(user: "visitante", password: "conheceruno").
        Alamofire.request(OAUTH_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData{ response in 
            //TRANSFORMANDO A DATA EM UM DICIONARIO
            
            do {
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableLeaves) as? NSDictionary
                
                if let parseJSON = json {
                    
                    token = (parseJSON["access_token"] as? String)!
                    type = (parseJSON["token_type"] as? String)!
                    
                }
            } catch {
                print("Erro ao pegar o token")
                
            }
            
            closure(type+" "+token)
        }
    }
    
    class func sendInfo(info: UserInfo, token: String) { //TROCAR PARA PASSAR O OBJETO POR PARAMETRO
//        let parameters : [String:Any] = ["nome": "User",
//                                            "email": "user@bol.com",
//                                            "ano": 3,
//                                            "escola": "Lara Ribas",
//                                            "cursos": [
//                                                "sistemas": 50,
//                                                "computacao":35,
//                                                "adm": 52,
//                                                "matematica": 45,
//                                                "eletrica": 23,
//                                                "quimica": 44,
//                                            ]
//                                        ]
 
        let parameters = try! JSONSerialization.jsonObject(with: (info.json?.data(using: .utf8))!, options: []) as! [String: Any]
        print("parameters: ", parameters)
        
        let headers: HTTPHeaders = [
            "Authorization": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(SERVICE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData{ response in
            //TRANSFORMANDO A DATA EM UM DICIONARIO
            print("*", token, response.response ?? "Null")
            do {
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    print(parseJSON)
                    if parseJSON["success"] as! String == "Dados armazenados com sucesso"{
                        getContext().delete(info)
                        
                        do{
                            try getContext().save()
                        }catch{
                            print("Erro aou salvar 45")
                        }
                    }
                }
            } catch {
                print("Server off")
            }
        }
    }
}

