//
//  AlternativaDAO.swift
//  TesteVocacionalDatabase
//
//  Created by Unochapeco unochapeco on 18/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData
import Google

class AlternativaDAO{
    class func criarAlternativa(texto : String, areas : [String:Int], carac: [String: Bool]) -> Alternativa{
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let alt = Alternativa(context: context)
        //SETANDO VARIÁVEIS
        alt.addCaracteristicas(caracteristicas: carac)
        alt.texto = texto
        alt.date = NSDate()
        
        for (area, pontos) in areas{
            
            let altArea = AlternativaArea(context: context)
            let a = AreaDAO.getArea(byName: area)
            altArea.alternativaArea_alternativa = alt
            altArea.alternativaArea_area = a
            altArea.pontos = Int16(pontos)
        }
        
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        //questao.addToQuestao_alternativa(r)
        
        return alt
    }
    
    class func assignToQuestion(alternativas: [Alternativa], questao: Questao) {
        for a in alternativas {
            questao.addToQuestao_alternativa(a)
        }
        do{
            try getContext().save()
        }catch{
            print("Erro ao salvar a escolha de uma alternativa")
        }
    }
    
    class func escolherAlternativa(alternativa : Alternativa){
        let usuario = UsuarioDAO.getUsuario()
        
        usuario?.addToUsuario_alternativa(alternativa)
        do{
            try getContext().save()
        }catch{
            print("Erro ao salvar a escolha de uma alternativa")
        }
        
    }
    
    class func getAlternativas(questao : Questao) -> [Alternativa]{
        let fetchRequest: NSFetchRequest<Alternativa> = Alternativa.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "alternativa_questao == %@", questao)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        var searchResults : [Alternativa] = []
        do {
            searchResults = try getContext().fetch(fetchRequest)
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return searchResults
    }
    
    class func deleteAlternativasSelecionadas() {
        let usuario = UsuarioDAO.getUsuario()
        let fetchRequest: NSFetchRequest<Alternativa> = Alternativa.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "alternativa_usuario == %@", UsuarioDAO.getUsuario()!)
        
        var searchResults = [Alternativa]()
        do {
            searchResults = try getContext().fetch(fetchRequest)
        } catch {
            print("Erro ao pegar alternativas")
        }
        for alt in searchResults {
            usuario?.removeFromUsuario_alternativa(alt)
        }
        
        do {
            try getContext().save()
        } catch {
            print("Erro ao tentar salvar #19")
        }
    }

    class func deleteRespostaQuestao(questao: Questao) {
        let usuario = UsuarioDAO.getUsuario()
        
        let fetchRequest: NSFetchRequest<Alternativa> = Alternativa.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "alternativa_usuario == %@ AND alternativa_questao == %@", usuario!, questao)
    
        var searchResults = [Alternativa]()
        do {
            searchResults = try getContext().fetch(fetchRequest)
        } catch {
            print("Erro ao pegar alternativas")
        }
        for alt in searchResults {
            usuario?.removeFromUsuario_alternativa(alt)
        }
        
        do {
            try getContext().save()
        } catch {
            print("Erro ao tentar salvar #19")
        }
    }

}
