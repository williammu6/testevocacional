//
//  Ambientais.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class Ambientais: SuperQuestao {

    class func criarQuestoes() {
        let questao = newQuestao(enunciado: "Você gosta do meio ambiente?", categoria: "Ambientais")
        let a = [
            newAlternativa(texto: "Sim", areas: [:], carac: ["social": true]),
            newAlternativa(texto: "Não",  areas: [:], carac: ["social": true])
        ]
        
        assign(alternativas: a, questao: questao)
    }
}
