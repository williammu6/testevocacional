//
//  AreaDAO.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 09/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class AreaDAO{
    class func criarArea(nome : String){
        
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let area = Area(context: context)
        //SETANDO VARIÁVEIS
        area.nome = nome
    
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getPontuacaoArea(area : Area) -> Double{
        var pontos: Double = 0
        
        let fetchRequestAA: NSFetchRequest<AlternativaArea> = AlternativaArea.fetchRequest()
                
        do{
            fetchRequestAA.predicate = NSPredicate(format: "alternativaArea_area == %@ AND alternativaArea_alternativa.alternativa_usuario == %@", area, UsuarioDAO.getUsuario()!)
            
            let AA = try getContext().fetch(fetchRequestAA) //TODAS ALTERNATIVAS QUE O USUÁRIO ESCOLHEU
            
            for i in AA{ //PARA CADA ALTERNATIVA CORRETA, VAMOS PROCURAR SUA PONTUAÇÃO PARA SOMAR
                
                pontos += Double(i.pontos)
                
            }
        }catch{
            print("Erro em pegar pontuação da area")
        }
        
        return Double(pontos) 
    }

    class func getPontuacaoArea(byName name: String) -> Double{
        let area = getArea(byName: name)
        
        return getPontuacaoArea(area: area)
    }
    
    class func getAreas() -> [Area] {
        let fetchRequest: NSFetchRequest<Area> = Area.fetchRequest()
        
        var areas: [Area] = []
        
        do {
            let lista = try getContext().fetch(fetchRequest) as [Area]
            if lista.count == 0 {
                print("mt loco vei fudeu geral")
            }
            
            areas = lista
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return areas
    }
    
    class func getArea(byName name: String?) -> Area{
    
        let fetchRequest: NSFetchRequest<Area> = Area.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nome == %@", name!)
        fetchRequest.fetchLimit = 1
        
        let context = getContext()
        
        var area : Area?
        
        do {
            let lista = try context.fetch(fetchRequest) as [Area]

            if lista.count == 0 {
                print("mt loco vei fudeu geral")
            }
            area = lista[0]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return area!
    }
    
    
    class func getPontuacaoAreas() -> [Area:Double]{
        var ret : [Area:Double] = [:]
        let cursos = getAreas()
        
        for a in cursos{
            let pontos = getPontuacaoArea(area: a)
            ret[a] = pontos
        }
        
        return ret
    }
}
