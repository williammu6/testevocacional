//
//  BarraView.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 02/02/17.
//  Copyright © 2017 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class BarraView: UIView {
    
    let logoImage: UIImageView = {
        let image = UIImageView(image: UIImage(named: "unochapeco"))
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let voltarButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "voltar"), for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        return bt
    }()
    
    let mainProfileButton: UIButton = {
        //IMAGEM DO USUÁRIO
        let bt = UIButton()
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.layer.masksToBounds = true
        bt.layer.borderWidth = 2.0
        bt.layer.borderColor = UIColor.white.cgColor
        return bt
    }()
    
    let configImage: UIImageView = {
        //IMAGEM DO USUÁRIO
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.masksToBounds = true
        //image.layer.borderWidth = 0.4
        //image.layer.borderColor = UIColor.white.cgColor
        image.image = UIImage(named: "configwhite")
        return image
    }()
    
    var voltarBloqueado = false;
    
    func setup(view: UIView){
        self.backgroundColor = UIColor.unoColor()
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        self.layer.masksToBounds = false
        self.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        self.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        self.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.layoutIfNeeded()
        
        self.addSubview(logoImage)
        logoSetup()
        
        self.addSubview(voltarButton)
        voltarSetup()
        
        self.addSubview(mainProfileButton)
        self.addSubview(configImage)
        catchUserDefaultsInfo()
        profileSetup()
        
        if(voltarBloqueado){
            voltarButton.setImage(UIImage(named: "voltarbloqueado"), for: .normal)
        }
        
        self.layoutIfNeeded()
        
    }
    
    private func logoSetup(){
        logoImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        logoImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        logoImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.7).isActive = true
        logoImage.widthAnchor.constraint(equalTo: logoImage.heightAnchor, multiplier: 2.7).isActive = true
        logoImage.layoutIfNeeded()
    }
    
    private func voltarSetup(){
        voltarButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        voltarButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        voltarButton.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5).isActive = true
        voltarButton.widthAnchor.constraint(equalTo: voltarButton.heightAnchor, multiplier: 1.4).isActive = true
        voltarButton.layoutIfNeeded()
    }
    
    private func profileSetup(){
        mainProfileButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        mainProfileButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        mainProfileButton.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.8).isActive = true
        mainProfileButton.widthAnchor.constraint(equalTo: mainProfileButton.heightAnchor).isActive = true
        
        self.layoutIfNeeded()
        mainProfileButton.layer.cornerRadius = mainProfileButton.frame.width/4
        mainProfileButton.layoutIfNeeded()
        
        configImage.leftAnchor.constraint(equalTo: mainProfileButton.centerXAnchor, constant: mainProfileButton.frame.width/10).isActive = true
        configImage.topAnchor.constraint(equalTo: mainProfileButton.centerYAnchor, constant: mainProfileButton.frame.height/10).isActive = true
        configImage.widthAnchor.constraint(equalTo: mainProfileButton.widthAnchor, multiplier: 1/2).isActive = true
        configImage.heightAnchor.constraint(equalTo: mainProfileButton.heightAnchor, multiplier: 1/2).isActive = true
        configImage.layoutIfNeeded()
        
    }
    
    private func catchUserDefaultsInfo() {
        if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: defaults.data(forKey: "userInfo")!) as? [String : Any?]{
            if let i = userInfo["image"]{
                mainProfileButton.setImage(i as? UIImage, for: .normal)
            } else{
                print("Error trying to catch user information")
                mainProfileButton.setImage(UIImage(named: "anonymous"), for: .normal)
            }
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
