//
//  CadastroAreas.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 09/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

//CRIANDO AREAS DOS CURSOS
class NewArea {
    class func criarAreas(){
        novaArea(area: "Exatas")
        novaArea(area: "Ambientais")
        novaArea(area: "Humanas")
        novaArea(area: "Saúde")
        novaArea(area: "Sociais")
    }
    
    class private func novaArea(area: String) {
        AreaDAO.criarArea(nome: area)
    }
}
