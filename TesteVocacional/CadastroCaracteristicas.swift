//
//  CadastroCaracteristicas.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 09/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

//CRIANDO CARACTERISTICAS DOS CURSOS
class NewCarac {
    static func criarCarac() {
        novaCarac(nome: "trabalhar_sentado")
        novaCarac(nome: "social")
        novaCarac(nome: "matematica")
        novaCarac(nome: "logica")
        novaCarac(nome: "versatilidade")
        novaCarac(nome: "dinamismo")
        novaCarac(nome: "comunicacao")
        novaCarac(nome: "criatividade")
        novaCarac(nome: "gostar_de_leite")
        novaCarac(nome: "lideranca")
    }
    class private func novaCarac(nome: String) {
        CaracteristicaDAO.criarCaracteristica(nome: nome)
    }
}
