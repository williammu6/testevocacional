//
//  CadastroCategoriaQuestao.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 09/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

//CRIANDO CATEGORIAS DE QUESTOES
class NewCategoriaQuestao {
    class func novaCategoria(){
        criarCategoria(nome: "genericas")
        criarCategoria(nome: "Exatas")
        criarCategoria(nome: "Ambientais")
        criarCategoria(nome: "Humanas")
        criarCategoria(nome: "Saúde")
        criarCategoria(nome: "Sociais")
    }
    
    class private func criarCategoria(nome: String) {
        CategoriaQuestaoDAO.criarCategoriaQuestao(nome: nome)
    }
}
