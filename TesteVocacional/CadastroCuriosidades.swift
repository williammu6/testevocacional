//
//  CadastroCuriosidades.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 28/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class NewCuriosidade{
    class func criarCuriosidades(){
        //GENÉRICAS
        criarCuriosidade(texto: "80% dos graduados na Unochapecó estão atuando no mercado de trabalho", area: "generica")
        
        //EXATAS
        criarCuriosidade(texto: "O CRS é mt legal", area: "Exatas")
        //HUMANAS
        criarCuriosidade(texto: "Sei lá, deve ter alguma coisa mais ou menos em humanas", area: "Humanas")

 
        // TESTES
        //exatas
        criarCuriosidade(texto: "Exatas 1", area: "Exatas")
        criarCuriosidade(texto: "Exatas 2", area: "Exatas")
        criarCuriosidade(texto: "Exatas 3", area: "Exatas")
        criarCuriosidade(texto: "Exatas 4", area: "Exatas")
        
        //Humanas
        criarCuriosidade(texto: "Humanas 1", area: "Humanas")
        criarCuriosidade(texto: "Humanas 2", area: "Humanas")
        criarCuriosidade(texto: "Humanas 3", area: "Humanas")
        criarCuriosidade(texto: "Humanas 4", area: "Humanas")
        criarCuriosidade(texto: "Humanas 5", area: "Humanas")
        
        //Sociais
        criarCuriosidade(texto: "Sociais 1", area: "Sociais")
        criarCuriosidade(texto: "Sociais 2", area: "Sociais")
        criarCuriosidade(texto: "Sociais 3", area: "Sociais")
        criarCuriosidade(texto: "Sociais 4", area: "Sociais")
        
    }
    class private func criarCuriosidade(texto: String, area: String){
        CuriosidadeDAO.criarCuriosidade(texto: texto, area: area)
    }
    
}
