//
//  CadastroCursos.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 09/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

let FORTE = 10
let BOA = 8
let MEDIA = 6
let BAIXA = 4

//CRIANDO CURSOS
class NewCurso {
    
    class func criarCursos(){
        //AMBIENTAIS
        newCurso(nome: "Ciências Biológicas", alias: "biologicas", area: "Ambientais")
        newCurso(nome: "Engenharia de Alimentos", alias: "alimentos", area: "Ambientais")
        newCurso(nome: "Medicina Veterinária", alias: "veterinaria", area: "Ambientais")
        newCurso(nome: "Produção Leiteira", alias: "leiteira", area: "Ambientais")
        
        //SOCIAIS APLICADAS
        newCurso(nome: "Administração", alias: "adm", area: "Sociais")
        newCurso(nome: "Ciências Contábeis", alias: "contabeis", area: "Sociais")
        newCurso(nome: "Ciências Econômicas", alias: "economia", area: "Sociais")
        newCurso(nome: "Design de Moda", alias: "moda", area: "Sociais")
        newCurso(nome: "Design", alias: "design", area: "Sociais")
        newCurso(nome: "Jogos Digitais", alias: "jogos", area: "Sociais")
        newCurso(nome: "Jornalismo", alias: "jornalismo", area: "Sociais")
        newCurso(nome: "Prodoção Audiovisual", alias: "audiovisual", area: "Sociais")
        newCurso(nome: "Publicidade e Propaganda", alias: "publicidade", area: "Sociais")

        //EXATAS
        newCurso(nome: "Ciência da Computação", alias: "computacao", area: "Exatas")
        newCurso(nome: "Engenharia Civil", alias: "civil", area: "Exatas")
        newCurso(nome: "Engenharia Elétrica", alias: "eletrica", area: "Exatas")
        newCurso(nome: "Engenharia de Producao", alias: "producao", area: "Exatas")
        newCurso(nome: "Engenharia Química", alias: "quimica", area: "Exatas")
        newCurso(nome: "Física", alias: "fisica", area: "Exatas")
        newCurso(nome: "Matemática", alias: "matematica", area: "Exatas")
        newCurso(nome: "Sistemas de Informação", alias: "sistemas", area: "Exatas")
        
        //HUMANAS
        newCurso(nome: "Artes Visuais", alias: "visuais", area: "Humanas")
        newCurso(nome: "Biblioteconomia", alias: "biblioteconomia", area: "Humanas")
        newCurso(nome: "Ciências da Religião", alias: "religiao", area: "Humanas")
        newCurso(nome: "Direito", alias: "direito", area: "Humanas")
        newCurso(nome: "Educação Especial", alias: "educacao_especial", area: "Humanas")
        newCurso(nome: "Intercultura Indígena", alias: "indigena", area: "Humanas")
        newCurso(nome: "Letras", alias: "letras", area: "Humanas")
        newCurso(nome: "Letras-Libras", alias: "libras", area: "Humanas")
        newCurso(nome: "Pedagogia", alias: "pedagogia", area: "Humanas")
        newCurso(nome: "Psicologia", alias: "psicologia", area: "Humanas")
        newCurso(nome: "Serviço Social", alias: "social", area: "Humanas")

        //SAÚDE
        newCurso(nome: "Educação Física", alias: "educacao_fisica", area: "Saúde")
        newCurso(nome: "Enfermagem", alias: "enfermagem", area: "Saúde")
        newCurso(nome: "Farmácia", alias: "farmacia", area: "Saúde")
        newCurso(nome: "Fisioterapia", alias: "fisioterapia", area: "Saúde")
        newCurso(nome: "Gastronomia", alias: "gastronomia", area: "Saúde")
        newCurso(nome: "Medicina", alias: "medicina", area: "Saúde")
        newCurso(nome: "Nutrição", alias: "nutricao", area: "Saúde")
        newCurso(nome: "Odontologia", alias: "odontologia", area: "Saúde")
        
        //SETTING CARACTERISTICAS
        
        //AMBIENTAIS
        setCaracteristicas(alias: "biologicas",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "alimentos",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "veterinaria", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "leiteira", carac:["gostar_de_leite":FORTE, "dinamismo":BOA])

        //SOCIAIS APLICADAS
        setCaracteristicas(alias: "adm",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "contabeis", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "economia",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "moda", carac:["criatividade": FORTE])
        setCaracteristicas(alias: "design", carac:["criatividade": FORTE])
        setCaracteristicas(alias: "jogos", carac:["logica": FORTE, "criatividade": FORTE])
        setCaracteristicas(alias: "jornalismo", carac:["comunicacao": FORTE])
        setCaracteristicas(alias: "audiovisual", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "publicidade", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])

        //EXATAS
        setCaracteristicas(alias: "computacao", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "civil", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "eletrica",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "producao",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "quimica", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "fisica", carac:["logica": FORTE])
        setCaracteristicas(alias: "matematica", carac:["matematica": FORTE, "logica": MEDIA])
        setCaracteristicas(alias: "sistemas", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])

        //HUMANAS
        setCaracteristicas(alias: "visuais", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "biblioteconomia", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "religiao", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "direito", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "educacao_especial", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "indigena",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "letras", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "libras",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "pedagogia", carac:["comunicacao": FORTE])
        setCaracteristicas(alias: "psicologia", carac:["comunicacao": FORTE])
        setCaracteristicas(alias: "social", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])

        //SAÚDE
        setCaracteristicas(alias: "educacao_fisica",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "enfermagem", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "farmacia",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "fisioterapia",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "gastronomia",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "medicina", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "nutricao",  carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
        setCaracteristicas(alias: "odontologia", carac:["trabalhar_sentado": FORTE, "logica": FORTE, "dinamismo": FORTE, "versatilidade": FORTE])
    }
    
    class private func newCurso(nome: String, alias: String, area: String) {
        CursoDAO.criarCurso(nome: nome, alias: alias, area: area)
    }
    class private func setCaracteristicas(alias: String, carac: [String: Int] ){
        CursoDAO.getCurso(byAlias: alias).addCaracteristicas(caracteristicas: carac)
    }
}
