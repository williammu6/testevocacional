//
//  CaracteristicaDAO.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 08/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class CaracteristicaDAO{
    class func criarCaracteristica(nome:String){
        let context = getContext()
        
        let caracteristica = Caracteristica(context: context)
        
        caracteristica.nome = nome
        
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getCaracteristica(byName name: String) -> Caracteristica{
        let fetchRequest: NSFetchRequest<Caracteristica> = Caracteristica.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nome == %@", name)
        fetchRequest.fetchLimit = 1
        var c : Caracteristica?
        do {
            let lista = try getContext().fetch(fetchRequest)
            c = lista[0]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return c!
    }
    
    class func getCaracteristicas(byCurso curso: Curso) -> [Caracteristica]{
        let fetchRequest: NSFetchRequest<Caracteristica> = Caracteristica.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "ANY caracteristica_caracteristicaCurso.caracteristicaCurso_curso == %@", curso)

        var c : [Caracteristica] = []
        do {
            let lista = try getContext().fetch(fetchRequest)
            c = lista
        } catch {
            print("Error with request: \(error)")
        }
        
        return c
    }
    
    class func getCaracteristicas(byAlternativa alternativa: Alternativa) -> [Caracteristica]{
        let fetchRequest: NSFetchRequest<Caracteristica> = Caracteristica.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "ANY caracteristica_caracteristicaAlternativa.caracteristicaAlternativa_alternativa == %@", alternativa)
        
        var c : [Caracteristica] = []
        do {
            let lista = try getContext().fetch(fetchRequest)
            c = lista
        } catch {
            print("Error with request: \(error)")
        }
        
        return c
    }
    
}
