//
//  CategoriaQuestao.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 30/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class CategoriaQuestaoDAO{
    class func criarCategoriaQuestao(nome : String){
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let catQuestao = CategoriaQuestao(context: context)
        //SETANDO VARIÁVEIS
        catQuestao.nome = nome
        
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getCategoria(byName name: String) -> CategoriaQuestao{
        let fetchRequest: NSFetchRequest<CategoriaQuestao> = CategoriaQuestao.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nome == %@", name)
        fetchRequest.fetchLimit = 1
        var categoria : CategoriaQuestao?
        do {
            let lista = try getContext().fetch(fetchRequest) as [CategoriaQuestao]
            categoria = lista[0]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return categoria!
    }
    
}
