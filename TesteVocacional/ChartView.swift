//
//  TestChartViewController.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 01/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit
import SpriteKit

class ChartView: UIView {

    var pontuacoes : [(key: String, value: Double)] = []
    var labelList = [UILabel]()
    
    func desenharChart(pontuacoes: [(key: String, value: Double)]){
        
        let pathAnim = UIBezierPath()
        let path = UIBezierPath()
        let pathMaior = UIBezierPath()
        
        let shapeMaior = CAShapeLayer()
        let shape = CAShapeLayer()
        let shapeAnim = CAShapeLayer()
                
        let menorEixo = self.frame.height < self.frame.width ? self.frame.height : self.frame.width
        
        let max : Double = Double(menorEixo/2.0)*6/8
        self.pontuacoes = pontuacoes
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        var angulo : Double = 1/Double(pontuacoes.count*2)
        var anguloLabel : CGFloat = CGFloat(-(360/(pontuacoes.count*2)))
        
        shape.path = path.cgPath
        
        self.layer.addSublayer(shapeMaior)
        self.layer.addSublayer(shape)
        self.layer.addSublayer(shapeAnim)
        
        shape.lineWidth = 2
        shape.lineJoin = kCALineJoinMiter
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.white.cgColor
        
        shapeMaior.lineWidth = 2
        shapeMaior.lineJoin = kCALineJoinMiter
        shapeMaior.strokeColor = UIColor.black.cgColor
        shapeMaior.fillColor = UIColor.red.cgColor
        
        let maior = maiorPontuacao()
        let multiplicador : Double = Double((max*7/9)/maior)
        
        let min = maior/4.5
        
        let centro = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        let first = self.min(valor: (pontuacoes.first?.value)!, min: min)
        
        pathAnim.move(to: CGPoint(x: Double(centro.x) + sin(degrees: angulo*360)*first*multiplicador,
                                  y: Double(centro.y) + cos(degrees: angulo*360)*first*multiplicador))
        
        path.move(to: CGPoint(x: centro.x, y: centro.y))
        
        pathMaior.move(to: CGPoint(x: Double(centro.x) + sin(degrees: angulo*360)*(max),
                                   y: Double(centro.y) + cos(degrees: angulo*360)*(max)))
       
        path.addLine(to: CGPoint(x: centro.x, y: centro.y))
        
        shape.path = path.cgPath
        
        
        
        for (k, p) in pontuacoes {
            
            let p2 = self.min(valor: p, min: min)
            
            pathAnim.addLine(to: CGPoint(x: Double(centro.x) + sin(degrees: angulo*360)*p2*multiplicador,
                                     y: Double(centro.y) + cos(degrees: angulo*360)*p2*multiplicador ))
            
            pathMaior.addLine(to: CGPoint(x: Double(centro.x) + sin(degrees: angulo*360)*(max),
                                          y: Double(centro.y) + cos(degrees: angulo*360)*(max)))
            
            let label = UILabel(frame: CGRect(x: Double(centro.x) + sin(degrees: angulo*360)*(max*9/8),
                                              y: Double(centro.y) + cos(degrees: angulo*360)*(max*9/8), width: 0, height: 0))
            label.numberOfLines = 0
            label.text = k
            label.font = UIFont(name: "Trebuchet MS", size: self.frame.height/20)
            label.textColor = .white
            label.sizeToFit()
            
            label.frame = CGRect(x: label.frame.minX - (label.frame.width)/2,
                                 y: label.frame.minY - (label.frame.height)/2, width: label.frame.width, height: label.frame.height)
            
            label.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            
            if anguloLabel <= -90 && anguloLabel >= -270 {
                label.transform = CGAffineTransform(rotationAngle: degreesToRadans(degrees: Double(180+anguloLabel)))
            } else {
                label.transform = CGAffineTransform(rotationAngle: degreesToRadans(degrees: Double(anguloLabel)))
            }
            
            
            self.addSubview(label)
            
            angulo+=1/Double(pontuacoes.count*2)*2
            anguloLabel = CGFloat(anguloLabel - CGFloat(360/pontuacoes.count))
            
            
            labelList.append(label)
        }
        
        pathMaior.addLine(to: CGPoint(x: Double(centro.x) + sin(degrees: angulo*360)*(max),
                                      y: Double(centro.y) + cos(degrees: angulo*360)*(max)))
        
        pathAnim.close()
        path.close()
        
        shapeMaior.path = pathMaior.cgPath
        
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = path.cgPath
        animation.toValue = pathAnim.cgPath
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        shape.add(animation, forKey: animation.keyPath)
        
        shape.layoutIfNeeded()
        shapeMaior.layoutIfNeeded()
        self.layoutIfNeeded()
    }
    func min(valor: Double, min: Double) -> Double {
        return valor > min ? valor : min
    }
    func degreesToRadans(degrees: Double)->CGFloat {
        return CGFloat(degrees * M_PI / 180 )
    }

    func sin(degrees: Double) -> Double {
        return __sinpi(degrees/180.0)
    }
    
    func cos(degrees: Double) -> Double {
        return __cospi(degrees/180.0)
    }
    
    func maiorPontuacao() -> Double {
        var cont : Double = 0
        for (_, j) in pontuacoes {
            if cont < j {
                cont = j
            }
        }
        return Double(cont)
    }
    func changeColorLabel(color: UIColor){
        for i in labelList {
            i.textColor = color
        }
    }
}


