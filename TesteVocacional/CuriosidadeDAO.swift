//
//  CuriosidadeDAO.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 28/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData
import Darwin

class CuriosidadeDAO{
    class func criarCuriosidade(texto : String, area: String){
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let curiosidade = Curiosidade(context: context)
        
        if area != "generica"{
            let area = AreaDAO.getArea(byName: area)
            curiosidade.curiosidade_area = area
        }
        
        //SETANDO VARIÁVEIS
        curiosidade.texto = texto
        
        curiosidade.date = NSDate()
        
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getCuriosidades(byAreas areas: [String]) -> [Curiosidade]{
        let fetchRequest: NSFetchRequest<Curiosidade> = Curiosidade.fetchRequest()
        var lista : [Curiosidade] = []
        var soma = 0
        var superlista : [[Curiosidade]] = []
        for i in 0..<areas.count{
            let area = areas[i]
            if area != "generica"{
                fetchRequest.predicate = NSPredicate(format: "curiosidade_area.nome == %@", area)
            }else{
                fetchRequest.predicate = NSPredicate(format: "curiosidade_area == NULL")
            }

            do {
                let l = try getContext().fetch(fetchRequest) as [Curiosidade]
                var k : [Curiosidade] = []
                k.append(contentsOf: l.sorted(by: {($0.date as! Date) < ($1.date as! Date)}))
                soma+=k.count
                superlista.append(k)
            } catch {
                print("Error with request: \(error)")
            }
        }
        
        for i in 0..<soma{
            for j in superlista{
                if i < j.count{
                    lista.append(j[i])
                }
            }
            
        }
        
        return lista
    }

}
