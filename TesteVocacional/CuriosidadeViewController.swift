//
//  ViewController.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/28/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//
//

import UIKit

class CuriosidadeViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var curiosidade: Curiosidade?
    
    let vsLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Você Sabia?"
        lb.textColor = .white
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let continuarButton: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("Continuar", for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.layer.cornerRadius = 5
        bt.layer.masksToBounds = true
        bt.layer.borderWidth = 1
        bt.layer.borderColor = UIColor.black.cgColor
        bt.setTitleColor(.black, for: .normal)
        bt.backgroundColor = .orange
        return bt
    }()
    let curiosidadeLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = .white
        lb.numberOfLines = 0
        lb.textAlignment = .justified
        return lb
    }()
    
    let curiosidadeView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(r: 110, g: 110, b: 110)
        view.layer.masksToBounds = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        curiosidadeLabel.text = curiosidade?.texto!
        
        view.backgroundColor = UIColor.unoColor()
        
        vsLabel.font = UIFont(name: "Trebuchet MS", size: FontManager.titulo())
        curiosidadeLabel.font = UIFont(name: "Trebuchet MS", size: FontManager.titulo())
        continuarButton.titleLabel?.font = UIFont(name: "Trebuchet MS", size: FontManager.subtitulo())
        
        view.addSubview(curiosidadeView)
        curiosidadeView.addSubview(curiosidadeLabel)
        view.addSubview(vsLabel)
        view.addSubview(continuarButton)
        
        setupConstraints()
        continuarButton.addTarget(self, action: #selector(buttonPress), for: .touchUpInside)
    }
    
    func setupConstraints() {
        
        vsLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/25).isActive = true
        vsLabel.heightAnchor.constraint(equalToConstant: view.frame.height/25).isActive = true
        vsLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        vsLabel.layoutIfNeeded()
        curiosidadeView.topAnchor.constraint(equalTo: vsLabel.bottomAnchor).isActive = true
        curiosidadeView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curiosidadeView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -view.frame.width/30).isActive = true
        curiosidadeView.bottomAnchor.constraint(equalTo: continuarButton.topAnchor, constant: -view.frame.height/50).isActive = true
        
        curiosidadeView.layoutIfNeeded()
        
        curiosidadeLabel.widthAnchor.constraint(equalTo: curiosidadeView.widthAnchor, constant: -curiosidadeView.frame.width/10).isActive = true
        curiosidadeLabel.heightAnchor.constraint(equalTo: curiosidadeView.heightAnchor, multiplier: 0.8).isActive = true
        curiosidadeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curiosidadeLabel.centerYAnchor.constraint(equalTo: curiosidadeView.centerYAnchor).isActive = true
        
        curiosidadeLabel.layoutIfNeeded()
        continuarButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height/10).isActive = true
        continuarButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 5/6).isActive = true
        continuarButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func buttonPress(){
        if let v = QuestaoManager.nextQuestao(){
            
            var numCuriosidade = defaults.integer(forKey: "curiosidade")
            numCuriosidade+=1
            
            defaults.set(numCuriosidade, forKey: "curiosidade")
            
            let transition = CATransition()
            transition.duration = 0.3
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            
            self.view.window!.layer.add(transition, forKey: kCATransition)
            DispatchQueue.main.async(execute:{})
            self.present(v, animated: false, completion: nil)
            
            return
        } else {
            print("nao tem mais")
        }
    }

    
}
