//
//  CursoDAO.swift
//  TesteVocacionalDatabase
//
//  Created by Unochapeco unochapeco on 18/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class CursoDAO{
    class func criarCurso(nome : String, alias: String, area: String){
        let context = getContext() //PEGANDO O CONTEXT ATUAL
    
        let curso = Curso(context: context)
        
        curso.nome = nome
        curso.alias = alias
        curso.curso_area = AreaDAO.getArea(byName: area)
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }

    }
    
    class func getPontuacaoCurso(curso : Curso) -> Double{
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        var pontos: Double = 0
        
        let fetchRequestC: NSFetchRequest<Caracteristica> = Caracteristica.fetchRequest()
        let fetchRequestA: NSFetchRequest<Alternativa> = Alternativa.fetchRequest()
        let fetchRequestCC: NSFetchRequest<CaracteristicaCurso> = CaracteristicaCurso.fetchRequest()
        let fetchRequestCA: NSFetchRequest<CaracteristicaAlternativa> = CaracteristicaAlternativa.fetchRequest()

        do{
            fetchRequestA.predicate = NSPredicate(format: "alternativa_usuario == %@", UsuarioDAO.getUsuario()!)
            let alternativas = try context.fetch(fetchRequestA) //TODAS ALTERNATIVAS QUE O USUÁRIO ESCOLHEU
        
            fetchRequestC.predicate = NSPredicate(format: "ANY caracteristica_caracteristicaCurso.caracteristicaCurso_curso == %@", curso)
            let caracteristicas = try context.fetch(fetchRequestC) //TODAS AS CARACTERISTICAS QUE O CURSO TEM
        
            fetchRequestCA.predicate = NSPredicate(format: "caracteristicaAlternativa_alternativa IN %@ AND caracteristicaAlternativa_caracteristica IN %@", alternativas, caracteristicas)

            let CA = try context.fetch(fetchRequestCA) //TODAS AS CARACTERISTICAATERNATIVA ENTRE AS DUAS TABELAS CORRETAS
            
            //print(curso.nome,alternativas.count, caracteristicas.count, CA.count)
            
            for i in CA{ //PARA CADA ALTERNATIVA CORRETA, VAMOS PROCURAR SUA PONTUAÇÃO PARA SOMAR
                let multiplicador : Double = i.positivo ? 1 : -1
            
                fetchRequestCC.predicate = NSPredicate(format: "ANY caracteristicaCurso_caracteristica.caracteristica_caracteristicaAlternativa == %@ AND caracteristicaCurso_curso == %@", i, curso)
                let final = try! context.fetch(fetchRequestCC)
                for j in final{
                    pontos += Double(j.pontos)*multiplicador
                }
            }
        }catch{
            print("Erro em pegar pontuação do curso")
        }
        if pontos < 0 {
            pontos = 0
        }
        return Double(pontos)
    }
    
    class func getPontuacaoCurso(byName name: String) -> Double{
        let curso = getCurso(byName: name)
        
        return getPontuacaoCurso(curso: curso)
    }
    
    class func getPontuacaoCurso(byAlias alias: String) -> Double{
        let curso = getCurso(byAlias: alias)
        
        return getPontuacaoCurso(curso: curso)
    }
    
    class func getCursos() -> [Curso]{
        let fetchRequest: NSFetchRequest<Curso> = Curso.fetchRequest()
        
        var lista : [Curso] = []
        do {
            lista = try getContext().fetch(fetchRequest) as [Curso]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return lista
    }
    
    class func getCurso(byName name: String) -> Curso{
        let fetchRequest: NSFetchRequest<Curso> = Curso.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nome == %@", name)
        fetchRequest.fetchLimit = 1
        var curso : Curso?
        do {
            let lista = try getContext().fetch(fetchRequest) as [Curso]
            curso = lista[0]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return curso!
    }
    
    class func getCurso(byAlias alias: String) -> Curso{
        let fetchRequest: NSFetchRequest<Curso> = Curso.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "alias == %@", alias)
        fetchRequest.fetchLimit = 1
        var curso : Curso?
        do {
            let lista = try getContext().fetch(fetchRequest) as [Curso]
            curso = lista[0]
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return curso!
    }
    
    class func getPontuacaoCursos() -> [Curso:Double]{
        var ret : [Curso:Double] = [:]
        let cursos = getCursos()
        
        for c in cursos{
            let pontos = getPontuacaoCurso(curso: c)
            ret[c] = pontos
        }
        
        return ret
    }
    
    class func getCursos(byAreas: [String]) -> [Curso]{
        let fetchRequest: NSFetchRequest<Curso> = Curso.fetchRequest()
        
        var str: String = ""
        
        for index in byAreas {
            str += "curso_area.nome == '\(index)' OR "
        }
        
        str += "1!=1"
        
        fetchRequest.predicate = NSPredicate(format: str)
        
        var results = [Curso]()
        
        do {
            results = try getContext().fetch(fetchRequest)
        } catch {
            print("Errororor")
        }
        for i in results {
            print(i.nome!)
        }
        return results
    }
    
    class func getPontuacaoCursos(byAreas: [String]) -> [Curso:Double]{
        let fetchRequest: NSFetchRequest<Curso> = Curso.fetchRequest()
        
        var str: String = ""

        for index in byAreas {
            str += "curso_area.nome == '\(index)' OR "
        }
        
        str += "1!=1"
        
        fetchRequest.predicate = NSPredicate(format: str)
        print(str)
        var results = [Curso]()
        var pontos: [Curso: Double] = [:]
        
        do {
            results = try getContext().fetch(fetchRequest)
            print("AQUI", results.count)
            for (x) in results {
                pontos[x] = Double(getPontuacaoCurso(curso: x))
            }
        } catch {
            print("Errororor")
        }
        
        return pontos
    }
}
