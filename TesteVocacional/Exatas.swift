//
//  Exatas.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class Exatas: SuperQuestao {
    
    class func criarQuestoes() {
        let questao1 = newQuestao(enunciado: "Quanto é 1 + 1?", categoria: "Exatas")
        let aq1 = [
            newAlternativa(texto: "2", areas: [:], carac: ["trabalhar_sentado": true, "logica": true, "matematica": true]),
            newAlternativa(texto: "10", areas: [:], carac: ["trabalhar_sentado": true, "logica": true, "matematica": true])
        ]
        assign(alternativas: aq1, questao: questao1)
        
        let questao2 = newQuestao(enunciado: "Como é a fórmula de bhaskara?", categoria: "Exatas")
        let aq2 = [
            newAlternativa(texto: "-b+-srqt((b*b) - 4.a.c)/2.a)", areas: [:], carac: ["matematica":true, "logica":true]),
            newAlternativa(texto: "Mas nem sei que diabo é isso", areas: [:], carac: ["matematica":true, "logica":true]),
        ]
        assign(alternativas: aq2, questao: questao2)
    }
}
