//
//  Extensions.swift
//  TesteVocacional
//
//  Created by uno2540 on 11/25/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit


extension UIColor {
    
    /*ColorLiteral = Very Useful If Ur A Dumb White Bit That Dont Know How To Use RGB System*/
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat){
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    class func unoColor() -> UIColor {
        return UIColor(r: 4, g: 59, b: 75)
    }
    
    class func backgroundColor() -> UIColor {
        return UIColor(r: 220, g: 220, b: 220)
    }
}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

extension UIImage {
    
    convenience init(view: UIView) {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { ctx in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        self.init(cgImage: (image.cgImage)!)
    }
}

extension UITextField {
    class func leftView(field: UITextField) {
        field.layoutIfNeeded()
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: field.frame.width*0.05, height: field.frame.height))
        field.leftViewMode = .always
    }
}


