//
//  ExtensionsModel.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 08/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

extension Curso{
    func addCaracteristicas(caracteristicas c: [String:Int]){
        let context = getContext()
        for (caracteristica, pontos) in c{
            let cc : CaracteristicaCurso = NSEntityDescription.insertNewObject(forEntityName: "CaracteristicaCurso", into: context) as! CaracteristicaCurso
            cc.caracteristicaCurso_curso = self
            cc.caracteristicaCurso_caracteristica = CaracteristicaDAO.getCaracteristica(byName: caracteristica)
            cc.pontos = Int16(pontos)
        }
    }
}

extension Alternativa{
    func addCaracteristicas(caracteristicas c:[String : Bool]){
        let context = getContext()
        
        for (caracteristica, positivo) in c {
            
            let carac = CaracteristicaDAO.getCaracteristica(byName: caracteristica)
            
            let caracAlternativa = CaracteristicaAlternativa(context: context)
            
            caracAlternativa.caracteristicaAlternativa_alternativa = self
            caracAlternativa.caracteristicaAlternativa_caracteristica = carac
            caracAlternativa.positivo = positivo
            
        }        
    }
}
