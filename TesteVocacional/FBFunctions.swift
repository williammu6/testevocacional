//
//  FBFunctions.swift
//  TesteVocacional
//
//  Created by uno2540 on 11/23/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import FBSDKLoginKit


func getFacebookInfo(completion: @escaping ([String:Any?]) -> Void){
    
    var info : [String:Any?] = [:]
    
    FBSDKGraphRequest(graphPath: "/me/picture", parameters: ["fields":"", "type": "large", "redirect": false]).start { (connection, result, error) in
        let res: [String : AnyObject]? = (result as AnyObject).value(forKey: "data") as? [String : AnyObject]
        
        if let data = res{
            if let urlString = data["url"] as? String {
                if let url = URL(string: urlString){
                    DispatchQueue.global().async {}
                    var i: [String: Any?] = [:]
                    i["photo"] = url

                    let imageData = try? Data(contentsOf: i["photo"] as! URL)
                    info["image"] = UIImage(data: imageData!)
                    
                    completion(info)
                }
            }
        }else{
            print("nao peguei a foto")
        }
    }
    FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"name, email"]).start { (connection, result, error) in
        if let res:[String:AnyObject] = result as? [String : AnyObject]{
            
            
            info["name"] = res["name"]
            info["email"] = res["email"]
            
            completion(info)
        }
    }
}

