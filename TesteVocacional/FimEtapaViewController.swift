//
//  FimEtapaViewController.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 13/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class FimEtapaViewController: TelaNormal {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let chartView = ChartView()
    let fim = FimTesteViewController()
    var pontuacoes : [String : Double] = [:]
    let defaults = UserDefaults()
    var melhoresareas : [String] = []
    
    //BOTÃO PARA PROSSEGUIR
    let btProxEtapa: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("Descubra o curso perfeito para você!", for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.backgroundColor = UIColor(r: 150, g: 150, b: 150)
        bt.layer.cornerRadius = 5
        bt.layer.masksToBounds = true
        bt.layer.borderWidth = 1
        bt.layer.borderColor = UIColor.black.cgColor
        bt.setTitleColor(.black, for: .normal)
        bt.backgroundColor = .orange
        return bt
    }()
    //LABEL
    let msgButton : UIButton = {
        let l = UIButton()
        l.titleLabel?.numberOfLines = 0
        l.backgroundColor = .gray
        l.setTitle("Parabéns! Você completou a primeira etapa do teste. Confira quais são suas melhores áreas de atuação:", for: .normal)
        l.titleLabel?.font = UIFont(name: "Arial", size: FontManager.subtitulo())
        l.setTitleColor(.white, for: .normal)
    
        l.titleLabel?.textAlignment = .left
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.unoColor()
        
        //msgButton.titleLabel?.font = UIFont(name: "Arial", size: self.view.frame.width/25)
        btProxEtapa.titleLabel?.font = UIFont(name: "Arial", size: self.view.frame.width/25)
        
        chartView.translatesAutoresizingMaskIntoConstraints = false
        
        
        view.addSubview(scrollView)
        scrollView.addSubview(chartView)
        scrollView.addSubview(btProxEtapa)
        scrollView.addSubview(msgButton)
        
        btProxEtapa.titleEdgeInsets = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        msgButton.titleEdgeInsets = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        
        msgButton.titleLabel?.setContentHuggingPriority(1000.0, for: .horizontal)
        msgButton.titleLabel?.setContentCompressionResistancePriority(1, for: .horizontal)

        btProxEtapa.addTarget(self, action: #selector(callFimTeste), for: .touchUpInside)
        
        self.setupConstraints()
        
        //PEGANDO AS MELHORES ÁREAS
        let pontos = AreaDAO.getPontuacaoAreas()
        
        var porcentagem : [Area:Double] = [:]
        
        var soma : Double = 0
        
        for i in pontos{
            soma += i.value
        }
        
        for c in pontos{
            porcentagem[c.key] = (Double(c.value)/soma)*100.0
        }
    
        for i in 0...3{
            if let melhor = getMaxArea(porcentagem: porcentagem){
                let index = porcentagem.index(forKey: melhor)
                if i == 2 && porcentagem[melhor]! < 25.0{
                    break;
                }
                
                porcentagem.remove(at: index!)
                melhoresareas.append(melhor.nome!) //FAZENDO A LISTA DAS MELHORES ÁREAS DO USUÁRIO
            }
        }
        
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: melhoresareas)
        defaults.set(data, forKey: "areas")
        
        var chartPontuacoes : [String: Double] = [:]
        
        for i in pontos{
            chartPontuacoes[i.key.nome!] = Double(i.value)
        }
        
        pontuacoes = chartPontuacoes
        
        self.chartView.desenharChart(pontuacoes: self.pontuacoes.sorted(by: {$0.key < $1.key}))
        
    }
    
    
    private func setupConstraints(){
        
        scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true

        
        msgButton.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/20).isActive = true
        msgButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 5/6).isActive = true
        msgButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        msgButton.layoutIfNeeded()
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            chartView.topAnchor.constraint(equalTo: msgButton.bottomAnchor, constant: view.frame.height/10) .isActive = true
            chartView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 4/5).isActive = true
            chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        } else {
            chartView.topAnchor.constraint(equalTo: msgButton.bottomAnchor, constant: view.frame.height/10) .isActive = true
            chartView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
            chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        }
        chartView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        chartView.layoutIfNeeded()
        
        btProxEtapa.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height/10).isActive = true
        btProxEtapa.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 5/6).isActive = true
        btProxEtapa.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        msgButton.heightAnchor.constraint(equalToConstant: CGFloat((msgButton.titleLabel?.frame.height)! + 20)).isActive = true
        msgButton.layoutIfNeeded()
    }
    func callFimTeste() {
        
        //Montar a nova lista de perguntas
        QuestaoManager.questoes = QuestaoDAO.getQuestoes(byCategorias: melhoresareas)
        QuestaoManager.curiosidades = CuriosidadeDAO.getCuriosidades(byAreas: melhoresareas)        
        defaults.set(0, forKey: "questao")
        defaults.set(0, forKey: "curiosidade")
        defaults.set(2, forKey: "etapa")
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        self.view.window!.layer.add(transition, forKey: kCATransition)

        present(QuestaoManager.nextQuestao()!, animated: false, completion: nil)
    }
  
    private func getMaxArea(porcentagem : [Area:Double]) -> Area?{
        var melhor: Area?
        var melhorporcentagem : Double = 0
        
        for i in porcentagem{
            if melhorporcentagem < i.value{
                melhor = i.key
                melhorporcentagem = i.value
            }
        }
        
        return melhor
    }
 
}
