//
//  FimTesteViewController.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/14/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class FimTesteViewController: UIViewController {
    let defaults = UserDefaults()
    let chartView = ChartView()
    var pontuacoes: [String: Double] = [:]
    
    let restartButton: UIButton = {
        let bt = UIButton()
        bt.setTitle("Refazer Teste", for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setTitleColor(.black, for: .normal)
        bt.backgroundColor = .orange
        return bt
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .orange
        chartView.translatesAutoresizingMaskIntoConstraints = false
        restartButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(chartView)
        view.addSubview(restartButton)
        
        chartView.backgroundColor = .gray
        setupConstraints()
        
        chartView.desenharChart(pontuacoes: pontuacoes.sorted(by: {$0.value < $1.value}))
        
        restartButton.addTarget(self, action: #selector(restartTeste), for: .touchUpInside)
    }
    
    private func setupConstraints(){
        view.layoutIfNeeded()
        
        chartView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/10) .isActive = true
        chartView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        chartView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        restartButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height/10).isActive = true
        restartButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 5/6).isActive = true
        restartButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        chartView.layoutIfNeeded()
    }
    func restartTeste() {
        defaults.set(0, forKey: "questao")
        defaults.set(1, forKey: "etapa")
        defaults.set(nil, forKey: "areas")
        AlternativaDAO.deleteAlternativasSelecionadas()
        present(InitViewController(), animated: true, completion: nil)
    }
}
