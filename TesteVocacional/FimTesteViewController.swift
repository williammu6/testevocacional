//
//  FimTesteViewController.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/14/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class FimTesteViewController: TelaNormal {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let defaults = UserDefaults()
    let chartView = ChartView()
    var pontuacoes: [String: Double] = [:]
    
    let restartButton: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("Refazer Teste", for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.layer.cornerRadius = 5
        bt.layer.masksToBounds = true
        bt.layer.borderWidth = 1
        bt.layer.borderColor = UIColor.black.cgColor
        bt.setTitleColor(.black, for: .normal)
        bt.backgroundColor = .orange
        return bt
    }()
    
    let shareButton: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("Share", for: .normal)
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.layer.cornerRadius = 5
        bt.layer.masksToBounds = true
        bt.layer.borderWidth = 1
        bt.layer.borderColor = UIColor.black.cgColor
        bt.setTitleColor(.black, for: .normal)
        bt.backgroundColor = .orange
        return bt
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = UIColor.unoColor()
        
        chartView.translatesAutoresizingMaskIntoConstraints = false
        restartButton.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(restartButton)
        view.addSubview(shareButton)
        view.addSubview(chartView)
        
        
        setupConstraints()
        //PEGANDO OS CURSOS
        
        var chartPontuacoes : [String: Double] = [:]
        
        let melhoresareas: [String] = NSKeyedUnarchiver.unarchiveObject(with: defaults.data(forKey: "areas")!) as! [String]
        let cursos = CursoDAO.getPontuacaoCursos(byAreas: melhoresareas)
        
        for p in cursos {
            chartPontuacoes[p.key.nome!] = p.value
        }
        
        let melhoresCursos: [String: Double] = getPontuacaoMelhoresCursos(cursos: cursos, alias: false)
        let melhoresCursosAlias: [String: Double] = getPontuacaoMelhoresCursos(cursos: cursos, alias: true)
        
        pontuacoes = melhoresCursos
        

        chartView.desenharChart(pontuacoes: pontuacoes.sorted(by: {$0.value < $1.value}))
        
        restartButton.addTarget(self, action: #selector(restartTeste), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(shareResult), for: .touchUpInside)
        
        let info = NSKeyedUnarchiver.unarchiveObject(with: defaults.data(forKey: "userInfo")!) as? [String : Any?]
        let moreInfo = NSKeyedUnarchiver.unarchiveObject(with: defaults.data(forKey: "moreInfo")!) as? [String: Any?]
        
        
        
        UserInfoDAO.criarUserInfo(nome: info!["name"] as! String, email: info!["email"] as! String, cursos: melhoresCursosAlias, escolaridade: (moreInfo?["escolaridade"] as! Int?) ?? 666, instituicao: (moreInfo?["instituicao"] as! String?) ?? "0")
        
    }
    
    private func setupConstraints(){
        view.layoutIfNeeded()
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            chartView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/10) .isActive = true
            chartView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 3/4).isActive = true
            chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        } else {
            chartView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/10) .isActive = true
            chartView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
            chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        }
        chartView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
       
        restartButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height/10).isActive = true
        restartButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 5/6).isActive = true
        restartButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        shareButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shareButton.bottomAnchor.constraint(equalTo: restartButton.topAnchor, constant: -view.frame.height/20).isActive = true
        shareButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/2).isActive = true
        
        chartView.layoutIfNeeded()
    }
    func restartTeste() {
        defaults.set(0, forKey: "questao")
        defaults.set(0, forKey: "curiosidade")
        QuestaoManager.curiosidadeDeveAparecer = true
        defaults.set(1, forKey: "etapa")
        defaults.set(true, forKey: "respondeu")
        defaults.set(nil, forKey: "areas")
        AlternativaDAO.deleteAlternativasSelecionadas()
        
        present(InitViewController(), animated: true, completion: nil)
    }
    
    func shareResult() {
        chartView.changeColorLabel(color: .black)
        let url = NSURL(string: "https://www.unochapeco.edu.br")
        let image = UIImage(view: chartView)
        
        let shareObjects: [AnyObject] = [url!, image]
        
        let activity = UIActivityViewController(activityItems: shareObjects, applicationActivities: nil)
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            if let popoverPresentationController = activity.popoverPresentationController{
                popoverPresentationController.sourceView = shareButton
                popoverPresentationController.sourceRect = shareButton.bounds
                popoverPresentationController.permittedArrowDirections = .down
                present(activity, animated: true, completion: nil)
            }
        } else {
            present(activity, animated: true, completion: nil)
        }
        chartView.changeColorLabel(color: .white)
    }
    
    //PEGAR 6 MELHORES CURSOS
    private func getPontuacaoMelhoresCursos(cursos: [Curso: Double], alias: Bool) -> [String: Double] {
        
        var melhoresCursos: [String:Double] = [:]
        var porcentagem: [Curso: Double] = [:]
        var soma: Double = 0
        
        for i in cursos{
            soma += i.value
        }
        //print("s",soma)
        for c in cursos{
            porcentagem[c.key] = (c.value/soma)*100.0
        }
        
        for _ in 0...5 {
            if let melhor = getMaxCurso(porcentagem: porcentagem){
                let index = porcentagem.index(forKey: melhor)
                porcentagem.remove(at: index!)
                let stringPos : String = alias ? melhor.alias! : melhor.nome!
                melhoresCursos[stringPos] = CursoDAO.getPontuacaoCurso(curso: melhor)//FAZENDO A LISTA DAS MELHORES ÁREAS DO USUÁRIO
            } else {
                print("Error ao pegar os 6 cursos")
//                let pont = CursoDAO.getPontuacaoCursos()
//                for (i, j) in pont{
//                    print(i, j)
//                }
            }
        }
        
        return melhoresCursos
    }
    
    private func getMaxCurso(porcentagem : [Curso:Double]) -> Curso?{
        var melhor: Curso?
        var melhorporcentagem : Double = 0
        
        for i in porcentagem{
            if melhorporcentagem < i.value{
                melhor = i.key
                melhorporcentagem = i.value
            }
        }
        return melhor
    }

}
