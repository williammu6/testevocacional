//
//  FontManager.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 26/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import UIKit

class FontManager{
    static var width : CGFloat = 0
    
    class func titulo() -> CGFloat{
        let screenSize: CGRect = UIScreen.main.bounds
        width = screenSize.width
        var tam : CGFloat = 0.0
        if width <= 320{ //IPHONE 5
            tam = 22
        } else if width <= 375{ //IPHONE 6
            tam = 25
        } else if width <= 414{ //IPHONE 6 PLUS
            tam = 28
        } else {
            tam = 34
        }
        return tam
    }
    
    class func subtitulo() -> CGFloat{
        let screenSize: CGRect = UIScreen.main.bounds
        width = screenSize.width
        var tam: CGFloat = 0.0
        if width <= 320{ //IPHONE 5
            tam = 18.5
        } else if width <= 375{ //IPHONE 6
            tam = 20
        } else if width <= 414{ //IPHONE 6 PLUS
            tam = 25
        } else {
            tam = 31
        }
        
        return tam
    }
    
    class func conteudo() -> CGFloat{
        let screenSize: CGRect = UIScreen.main.bounds
        width = screenSize.width
        var tam : CGFloat = 0.0
        if width <= 320{ //IPHONE 5
            tam = 17.5
        } else if width <= 375{ //IPHONE 6
            tam = 19
        } else if width <= 414{ //IPHONE 6 PLUS
            tam = 24
        } else {
            tam = 29
        }
        return tam
    }
}
