//
//  Genericas.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class Genericas: SuperQuestao {
    
    class func criarQuestoes() {
        /*let questao1 = newQuestao(enunciado: "Na escola, sua matéria favorita esta entre:", categoria: "genericas")
        let aq1 = [
            newAlternativa(texto: "Matemática, física ou química", areas: ["Exatas":FORTE, "Ambientais":BOA, "Saúde": BAIXA], carac: [:]),
            newAlternativa(texto: "História, geografia ou sociologia", areas: ["Humanas":BOA, "Sociais": MEDIA],carac: [:]),
            newAlternativa(texto: "Artes, educação física", areas: ["Saúde":BOA, "Sociais": MEDIA],carac: [:]),
            newAlternativa(texto: "Ciências humanas, idiomas", areas: ["Humanas":FORTE],carac: [:])
        ]
        
        //QUESTAO2:
        let questao2 = QuestaoDAO.criarQuestao(enunciado: "Você teria problema em trabalhar em equipe?", categoria: "genericas")
        let aq2 = [
            newAlternativa(texto: "Não teria", areas: ["Sociais":FORTE, "Humanas":MEDIA, "Saúde": BAIXA],carac: [:]),
            newAlternativa(texto: "Me adaptaria", areas: ["Exatas":BOA],carac: [:]),
            newAlternativa(texto: "Talvez", areas: ["Ambientais":BAIXA, "Exatas": BAIXA],carac: [:]),
            newAlternativa(texto: "Teria", areas: ["Saúde":BAIXA],carac: [:])
        ]
        
        //QUESTAO3:
        let questao3 = QuestaoDAO.criarQuestao(enunciado: "Como seria o trabalho ideal?", categoria: "genericas")

        let aq3 = [
            newAlternativa(texto: "Com regras e disciplina", areas: ["Humanas":FORTE,"Sociais":MEDIA,"Saúde":BOA],carac: [:]),
            newAlternativa(texto: "Sem muitas regras", areas: ["Exatas":MEDIA],carac: [:]),
            newAlternativa(texto: "Bastante autonomia", areas: ["Exatas":FORTE, "Sociais":BOA],carac: [:]),
            newAlternativa(texto: "Interagindo com outras pessoas", areas: ["Humanas":FORTE, "Sociais":FORTE, "Saúde":BOA],carac: [:])
        ]
        
        assign(alternativas: aq1, questao: questao1)
        assign(alternativas: aq2, questao: questao2)
        assign(alternativas: aq3, questao: questao3)*/
        
        let questao1 = newQuestao(enunciado: "Areas 0", categoria: "genericas")
        let aq1 = [
            newAlternativa(texto: "Exatas+10, Ambientais+8, Saúde+4", areas: ["Exatas":FORTE, "Ambientais":BOA, "Saúde": BAIXA], carac: [:]),
            newAlternativa(texto: "Humanas+8, Sociais+6", areas: ["Humanas":BOA, "Sociais": MEDIA],carac: [:]),
            newAlternativa(texto: "Saúde+8, Sociais+8", areas: ["Saúde":BOA, "Sociais": MEDIA],carac: [:]),
            newAlternativa(texto: "Humanas+10", areas: ["Humanas":FORTE],carac: [:])
        ]
        
        //QUESTAO2:
        let questao2 = QuestaoDAO.criarQuestao(enunciado: "Areas 1", categoria: "genericas")
        let aq2 = [
            newAlternativa(texto: "Sociais+10, Humanas+6, Saúde+4", areas: ["Sociais":FORTE, "Humanas":MEDIA, "Saúde": BAIXA],carac: [:]),
            newAlternativa(texto: "Exatas+8", areas: ["Exatas":BOA],carac: [:]),
            newAlternativa(texto: "Ambientais+4, Exatas+4", areas: ["Ambientais":BAIXA, "Exatas": BAIXA],carac: [:]),
            newAlternativa(texto: "Saúde+4", areas: ["Saúde":BAIXA],carac: [:])
        ]
        
        //QUESTAO3:
        let questao3 = QuestaoDAO.criarQuestao(enunciado: "Areas 2", categoria: "genericas")
        
        let aq3 = [
            newAlternativa(texto: "Humanas+10, Sociais+6, Saúde+8", areas: ["Humanas":FORTE,"Sociais":MEDIA,"Saúde":BOA],carac: [:]),
            newAlternativa(texto: "Exatas+6", areas: ["Exatas":MEDIA],carac: [:]),
            newAlternativa(texto: "Exatas+10, Sociais+6", areas: ["Exatas":FORTE, "Sociais":BOA],carac: [:]),
            newAlternativa(texto: "Humanas+10, Sociais+10, Saúde+8", areas: ["Humanas":FORTE, "Sociais":FORTE, "Saúde":BOA],carac: [:])
        ]
        
        assign(alternativas: aq1, questao: questao1)
        assign(alternativas: aq2, questao: questao2)
        assign(alternativas: aq3, questao: questao3)

    }
}

/*
    
 */
