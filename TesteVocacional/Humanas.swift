//
//  Humanas.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class Humanas: SuperQuestao {
    
    class func criarQuestoes() {
        let questao1 = newQuestao(enunciado: "Qual a diferença na pronúncia das palavras to, chew e two'", categoria: "Humanas")
        let aq1 =  [
            newAlternativa(texto: "Nenhuma", areas: [:], carac: ["logica":true]),
            newAlternativa(texto: "Não sei", areas: [:], carac: ["versatilidade": true])
        ]
        
        assign(alternativas: aq1, questao: questao1)
    }
}
