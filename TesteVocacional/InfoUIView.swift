//
//  InfoUIView.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/2/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class InfoUIView: UIView {
    
    //    func showMore() {
    //
    //        var pontos = [String: Double]()
    //
    //        // SOLICITAR AREAS CADASTRADAS
    //        let areas = AreaDAO.getAreas()
    //
    //        for i in areas {
    //            let pontosArea = AreaDAO.getPontuacaoArea(area: i)
    //            pontos[i.nome!] = Double(pontosArea)
    //        }
    //
    //        self.infoView.chartView.desenharChart(pontuacoes: pontos.sorted(by: {$0 < $1}))
    //        UIView.animate(withDuration: 0.1, animations: {
    //            self.infoView.alpha = 1
    //            self.alternativasView.alpha = 0
    //            self.questaoView.alpha = 0
    //            self.mainProfileImage.alpha = 0
    //            self.infoView.isHidden = false
    //        }, completion: { finished in
    //            self.alternativasView.isHidden = true
    //            self.questaoView.isHidden = true
    //            self.mainProfileImage.isHidden = true
    //        })
    //    }
    
    //    func showLess() {
    //        UIView.animate(withDuration: 0.1, animations: {
    //            self.mainProfileImage.alpha = 1
    //            self.alternativasView.alpha = 1
    //            self.questaoView.alpha = 1
    //            self.questaoView.isHidden = false
    //            self.alternativasView.isHidden = false
    //            self.mainProfileImage.isHidden = false
    //            self.infoView.alpha = 0
    //        }, completion: { finished in
    //            self.infoView.isHidden = true
    //        })
    //    }
    
    let defaults = UserDefaults()
    var conta = Int()
    
    let chartView = ChartView()
    
    func setup () {
        self.backgroundColor = UIColor(r: 100, g: 100, b: 100)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.zPosition = 2
        
        self.layer.cornerRadius = 10
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 5
        self.layer.shadowOffset = CGSize(width: 2, height: 7)
        
        self.layer.masksToBounds = false
        
        chartView.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(userImageView)
        self.addSubview(userNameLabel)
        self.addSubview(closeInfo)
        self.addSubview(chartView)
        
        self.addSubview(signOutButton)
        self.addSubview(signInButton)
        
        if(self.defaults.bool(forKey: "logado")) {
            self.signOutButton.isHidden = false
        } else {
            self.signInButton.isHidden = false
        }
    }
    
    let userImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.masksToBounds = true
        image.layer.borderWidth = 3.0
        image.layer.borderColor = UIColor.white.cgColor
        return image
    }()
    
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = UIFont(name: "Trebuchet MS", size: 30)
        
        return label
    }()
    
    let signOutButton: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Sign Out", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor(r: 50, g: 50, b: 50)
        btn.layer.cornerRadius = 7
        btn.isHidden = true
        btn.layer.masksToBounds = true
        return btn
    }()
    
    let signInButton: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Sign In", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor(r: 50, g: 50, b: 50)
        btn.layer.cornerRadius = 7
        btn.isHidden = true
        btn.layer.masksToBounds = true
        return btn
    }()
    
    let closeInfo: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "close")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    func setupConstraints() {
        self.layoutIfNeeded()
        
        userImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        userImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: self.frame.height/40).isActive = true
        userImageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/4).isActive = true
        userImageView.heightAnchor.constraint(equalTo: userImageView.widthAnchor).isActive = true
        
        userNameLabel.topAnchor.constraint(equalTo: userImageView.bottomAnchor, constant: 10).isActive = true
        userNameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        signOutButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -self.frame.height/100).isActive = true
        signOutButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        signOutButton.widthAnchor.constraint(equalTo: userImageView.widthAnchor).isActive = true
        
        signInButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -self.frame.height/100).isActive = true
        signInButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        signInButton.widthAnchor.constraint(equalTo: userImageView.widthAnchor).isActive = true
        
        chartView.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor, constant: self.frame.height/14).isActive = true
        chartView.widthAnchor.constraint(equalToConstant: self.frame.height*2/3.5 ).isActive = true
        chartView.heightAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        chartView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        closeInfo.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        closeInfo.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        closeInfo.heightAnchor.constraint(equalToConstant: 40).isActive = true
        closeInfo.widthAnchor.constraint(equalTo: closeInfo.heightAnchor).isActive = true
        
        chartView.layoutIfNeeded()
        self.layoutIfNeeded()
    }
}
