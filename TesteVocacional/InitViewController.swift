//
//  InitViewController.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 22/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit
import CoreData

func getContext () -> NSManagedObjectContext {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate.persistentContainer.viewContext
}

class InitViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let defaults = UserDefaults()
    let context = getContext()
    
    let startButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Começar", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: FontManager.titulo())
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor(r: 100, g: 100, b: 100)
        btn.layer.cornerRadius = 7
        btn.layer.masksToBounds = true
        return btn
    }()
    
    let logoImage: UIImageView = {
        let image = UIImageView(image: UIImage(named: "unochapeco"))
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let testeVocacionalLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste Vocacional"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        label.font = UIFont(name: "Helvetica Neue", size: FontManager.titulo())
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = UIColor.unoColor()
        
        //SUBVIEWS
        view.addSubview(startButton)
        view.addSubview(logoImage)
        view.addSubview(testeVocacionalLabel)
        
        startButton.addTarget(self, action: #selector(callViewController), for: .touchUpInside)
        
        //CALLING CONSTRAINTS
        
        setupLogoImage()
        setupStartButton()
        setupTesteLabel()
        
        if Network.isInternetAvailable() {
            APIServer.requestToken(closure: { token in
                if token == " " {
                    print("Sem Token")
                    return
                }
                for i in UserInfoDAO.getUserInfo() {
                    print("Com Token")
                    APIServer.sendInfo(info: i, token: token)
                }
            })
        }
        
        if defaults.bool(forKey: "cadastrado") != true {
            Setup.All()
            print("All")
            defaults.set(true, forKey: "respondeu")
            defaults.set(0, forKey: "questao")
            defaults.set(1, forKey: "etapa")
            defaults.set(0, forKey: "curiosidade")
        }
        
        if defaults.integer(forKey: "etapa") == 1{
            QuestaoManager.questoes = QuestaoDAO.getQuestoes(byCategorias: ["genericas"])
            QuestaoManager.curiosidades = CuriosidadeDAO.getCuriosidades(byAreas: ["generica"])
        } else if defaults.integer(forKey: "etapa") == 2{
            print("CARREGOU A 2")
            let unarchive: Data = defaults.data(forKey: "areas")!
            let melhoresareas: [String] = NSKeyedUnarchiver.unarchiveObject(with: unarchive) as! [String]
            QuestaoManager.questoes = QuestaoDAO.getQuestoes(byCategorias: melhoresareas)
            QuestaoManager.curiosidades = CuriosidadeDAO.getCuriosidades(byAreas: melhoresareas)
        }
        
    }
    
    func callViewController(startButton: UIButton) {
        startButton.setTitleColor(UIColor.white, for: .highlighted)
        if defaults.bool(forKey: "logado") {
            let i = self.defaults.integer(forKey: "questao")
            print("Questao Numero \(i)")
            present(QuestaoManager.nextQuestao()!, animated: true, completion: nil)
        } else {
            print("Nobody is logged in")
            present(LogInViewController(), animated: true, completion: nil)
        }
    }
    
    func setupLogoImage() {
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height/6).isActive = true
        logoImage.heightAnchor.constraint(equalTo: logoImage.widthAnchor, multiplier: 2/5).isActive = true
        logoImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 4/5).isActive = true
    }
    
    func setupTesteLabel() {
        testeVocacionalLabel.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: view.frame.height/10).isActive = true
        testeVocacionalLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func setupStartButton() {
        startButton.topAnchor.constraint(equalTo: testeVocacionalLabel.bottomAnchor, constant: view.frame.height/10).isActive = true
        startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        startButton.widthAnchor.constraint(equalTo: logoImage.widthAnchor).isActive = true
        startButton.heightAnchor.constraint(equalTo: logoImage.heightAnchor, multiplier: 1/3).isActive = true
    }
}




