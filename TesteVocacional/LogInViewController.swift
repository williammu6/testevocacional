//
//  LogInViewController.swift
//  TesteVocacional
//
//  Created by uno2540 on 11/22/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit
import GoogleSignIn
import GGLCore
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class LogInViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKLoginButtonDelegate, UITextFieldDelegate {
    
    let defaults = UserDefaults()
    
    let scrollView = UIScrollView()
    
    var heightAnchorRef : NSLayoutConstraint?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    enum Conta : Int{
        case google = 1, facebook = 2
    }
    
    var userInfo:[String:Any?] = [:]
    var cont = 0
    
    let googleButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "google"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let facebookButton: FBSDKLoginButton = {
        let btn = FBSDKLoginButton()
        btn.readPermissions = ["email"]
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let skipButton: UIButton = {
        var btn = UIButton(type: .system)
        btn.setTitle("Pular", for: .normal)
        btn.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        btn.setTitleColor(.black, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.layer.borderColor = UIColor(r: 150, g: 150, b: 150).cgColor
        btn.layer.borderWidth = 1
        
        return btn
    }()
    
    let logoImage: UIImageView = {
        var img = UIImageView(image: UIImage(named: "unochapeco"))
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let orLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "ou"
        lb.textColor = .black
        lb.textAlignment = .center
        return lb
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Nome"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        return tf
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        
        tf.backgroundColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let warningLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Verifique nome e endereço de email"
        lb.textColor = .red
        lb.isHidden = true
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.delegate = self
        self.nameTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        view.backgroundColor = scrollView.backgroundColor
        
        view.addSubview(scrollView)
        scrollView.addSubview(googleButton)
        scrollView.addSubview(facebookButton)
        scrollView.addSubview(skipButton)
        scrollView.addSubview(emailTextField)
        scrollView.addSubview(nameTextField)
        scrollView.addSubview(orLabel)
        scrollView.addSubview(warningLabel)
        scrollView.addSubview(logoImage)
        
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: view.heightAnchor)
        heightAnchorRef?.isActive = true
        scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        scrollView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        //        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //        scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        scrollView.layoutIfNeeded()
        var error: NSError?
        
        facebookButton.delegate = self
        
        GGLContext.sharedInstance().configureWithError(&error)
        if error != nil {
            print (error!)
            return
        }
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        googleButton.addTarget(self, action: #selector(loginGoogle), for: .touchUpInside)
        
        skipButton.addTarget(self, action: #selector(skipLogin), for: .touchUpInside)
        
        setupButtons()
        
        UITextField.leftView(field: nameTextField)
        UITextField.leftView(field: emailTextField)
    }
    
    //MARK: - Func
    
    func loginGoogle() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print(error)
            return
        }
        var i: [String: Any?] = [:]
        
        userInfo["name"] = user.profile.name
        userInfo["email"] = user.profile.email
        i["photo"] = user.profile.imageURL(withDimension: 400)
        print(userInfo)
        let imageData = try? Data(contentsOf: i["photo"] as! URL)
        userInfo["image"] = UIImage(data: imageData!)

        let data: Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
        
        self.defaults.set(data, forKey: "userInfo")
        self.defaults.set(Conta.google.rawValue, forKey:"conta")
        
        self.defaults.synchronize()
        
        self.showInfo()
    }
    
    //Essa função é chamada toda vez que o botão do facebook é chamado
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error!)
            return
        }
        getFacebookInfo { (info) in
            
            self.userInfo = info
            self.cont += 1
            
            if(self.userInfo["email"] != nil && self.userInfo["image"] != nil && self.userInfo["name"] != nil){
                print("Got enough info")
               
                print(self.userInfo)
                
                self.defaults.set(Conta.facebook.rawValue, forKey:"conta")
                let data : Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
                self.defaults.set(data, forKey:"userInfo")
                self.defaults.synchronize()
                self.showInfo()
            } else  {
                print("Didn't get enough info")
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {}
    
    func setupButtons() {
        
        
        facebookButton.layoutIfNeeded()
        googleButton.layoutIfNeeded()
        facebookButton.layoutIfNeeded()
        view.layoutIfNeeded()
        orLabel.layoutIfNeeded()
        nameTextField.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        warningLabel.layoutIfNeeded()
        
        facebookButton.titleLabel?.font = UIFont(name: "", size: scrollView.frame.height/30)
        
        googleButton.widthAnchor.constraint(equalToConstant: scrollView.frame.width*0.7).isActive = true
        googleButton.heightAnchor.constraint(equalTo: googleButton.widthAnchor, multiplier: 1/5).isActive = true
        googleButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        googleButton.topAnchor.constraint(equalTo: facebookButton.bottomAnchor, constant: 10).isActive = true
        
        facebookButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 100).isActive = true
        facebookButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        facebookButton.widthAnchor.constraint(equalTo: googleButton.widthAnchor, constant: -3).isActive = true
        facebookButton.heightAnchor.constraint(equalTo: googleButton.heightAnchor).isActive = true
        
        nameTextField.topAnchor.constraint(equalTo: googleButton.bottomAnchor, constant: scrollView.frame.height/4).isActive = true
        nameTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.7).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: scrollView.frame.height*0.05).isActive = true
        
        orLabel.bottomAnchor.constraint(equalTo: nameTextField.topAnchor, constant: -scrollView.frame.height/10).isActive = true
        orLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        orLabel.widthAnchor.constraint(equalTo: skipButton.widthAnchor, multiplier: 0.7).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: scrollView.frame.height/30).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: nameTextField.widthAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalTo: nameTextField.heightAnchor).isActive = true
        
        skipButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        skipButton.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 20).isActive = true
        skipButton.heightAnchor.constraint(equalToConstant: scrollView.frame.height/30).isActive = true
        skipButton.widthAnchor.constraint(equalTo: skipButton.heightAnchor, multiplier: 5/1).isActive = true
        
        skipButton.layoutIfNeeded()

        
        scrollView.contentSize.height = skipButton.frame.maxY + 10
        
        warningLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        warningLabel.topAnchor.constraint(equalTo: skipButton.bottomAnchor, constant: scrollView.frame.height/18).isActive = true
    
        logoImage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -scrollView.frame.height/20).isActive = true
        logoImage.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        logoImage.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1/3).isActive = true
        logoImage.heightAnchor.constraint(equalTo: logoImage.widthAnchor, multiplier: 1/2.5).isActive = true
    }
    
    func showInfo() {
//        AlternativaDAO.deleteAlternativasSelecionadas()
        self.defaults.set(true, forKey: "logado")
        present(QuestaoManager.nextQuestao()!, animated: true, completion: nil)
    }
    
    func skipLogin() {
        //AlternativaDAO.deleteAlternativasSelecionadas()
        if nameTextField.text! == "" || !isValidEmail(testStr: emailTextField.text!){
            print("Preencha os campos ou faça Login")
            shake()
            return
        } else {
            let email = emailTextField.text?.lowercased()
            self.userInfo["image"] = UIImage(named: "anonymous")!
            self.userInfo["name"] = nameTextField.text!
            self.userInfo["email"] = email!
            self.userInfo["photo"] = nil
            
            self.defaults.synchronize()
            
            let data : Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
            self.defaults.set(data, forKey: "userInfo")
            
            print("Info ", self.userInfo)
            
            present(QuestaoManager.nextQuestao()!, animated: true, completion: nil)
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        warningLabel.isHidden = false
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(animation, forKey: "position")
    }
    
    func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        heightAnchorRef?.isActive = false
        scrollView.removeConstraint(heightAnchorRef!)
        
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, constant: -keyboardFrame.height)
        heightAnchorRef?.isActive = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            //self.scrollView.frame.origin.y = self.enviarButton.frame.minY - keyboardFrame.height
            
        }, completion: { finished in
            self.emailTextField.layoutIfNeeded()
            
            self.scrollView.contentSize.height = self.skipButton.frame.maxY+10
            self.scrollView.setContentOffset(CGPoint(x: 0.0, y: keyboardFrame.height-(self.view.frame.maxY-self.skipButton.frame.maxY)+20), animated: true)
        })
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        heightAnchorRef?.isActive = false
        scrollView.removeConstraint(heightAnchorRef!)
        
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, constant: 0)
        heightAnchorRef?.isActive = true
        scrollView.layoutIfNeeded()
        
        scrollView.contentSize.height = skipButton.frame.maxY+10
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
