//
//  LogInViewController.swift
//  TesteVocacional
//
//  Created by uno2540 on 11/22/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit
import GoogleSignIn
import GGLCore
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class LogInViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKLoginButtonDelegate {
    
    let defaults = UserDefaults()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    enum Conta : Int{
        case google = 1, facebook = 2
    }
    
    var userInfo:[String:Any?] = [:]
    var cont = 0
    
    let googleButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "google"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let facebookButton: FBSDKLoginButton = {
        let btn = FBSDKLoginButton()
        btn.readPermissions = ["email"]
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let skipButton: UIButton = {
        var btn = UIButton(type: .system)
        btn.setTitle("Entrar", for: .normal)
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.layer.borderColor = UIColor(r: 150, g: 150, b: 150).cgColor
        btn.layer.borderWidth = 1
        
        return btn
    }()
    
    let orLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "ou"
        lb.textColor = .black
        lb.textAlignment = .center
        return lb
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Nome"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        return tf
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.backgroundColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let warningLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Verifique nome e endereço de email"
        lb.textColor = .red
        lb.isHidden = true
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    
        view.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        
        view.addSubview(googleButton)
        view.addSubview(facebookButton)
        view.addSubview(skipButton)
        view.addSubview(emailTextField)
        view.addSubview(nameTextField)
        view.addSubview(orLabel)
        view.addSubview(warningLabel)
        
        var error: NSError?
        
        facebookButton.delegate = self
        
        GGLContext.sharedInstance().configureWithError(&error)
        if error != nil {
            print (error!)
            return
        }
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        googleButton.addTarget(self, action: #selector(loginGoogle), for: .touchUpInside)
        
        skipButton.addTarget(self, action: #selector(skipLogin), for: .touchUpInside)
        
        setupButtons()
        
        UITextField.leftView(field: nameTextField)
        UITextField.leftView(field: emailTextField)
    }
    
    //MARK: - Func
    
    func loginGoogle() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print(error)
            return
        }
        var i: [String: Any?] = [:]
        
        userInfo["name"] = user.profile.name
        userInfo["email"] = user.profile.email
        i["photo"] = user.profile.imageURL(withDimension: 400)
        print(userInfo)
        let imageData = try? Data(contentsOf: i["photo"] as! URL)
        userInfo["image"] = UIImage(data: imageData!)

        let data: Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
        
        self.defaults.set(data, forKey: "userInfo")
        self.defaults.set(Conta.google.rawValue, forKey:"conta")
        
        self.defaults.synchronize()
        
        self.showInfo()
    }
    
    //Essa função é chamada toda vez que o botão do facebook é chamado
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error!)
            return
        }
        getFacebookInfo { (info) in
            
            self.userInfo = info
            self.cont += 1
            
            if(self.userInfo["email"] != nil && self.userInfo["image"] != nil && self.userInfo["name"] != nil){
                print("Got enough info")
               
                print(self.userInfo)
                
                self.defaults.set(Conta.facebook.rawValue, forKey:"conta")
                let data : Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
                self.defaults.set(data, forKey:"userInfo")
                self.defaults.synchronize()
                self.showInfo()
            } else  {
                print("Didn't get enough info")
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {}
    
    func setupButtons() {
        
        facebookButton.titleLabel?.font = UIFont(name: "", size: view.frame.height/30)
        
        googleButton.widthAnchor.constraint(equalToConstant: view.frame.width*0.7).isActive = true
        googleButton.heightAnchor.constraint(equalTo: googleButton.widthAnchor, multiplier: 1/5).isActive = true
        googleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        googleButton.topAnchor.constraint(equalTo: facebookButton.bottomAnchor, constant: 10).isActive = true
        
        facebookButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        facebookButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        facebookButton.widthAnchor.constraint(equalTo: googleButton.widthAnchor, constant: -3).isActive = true
        facebookButton.heightAnchor.constraint(equalTo: googleButton.heightAnchor).isActive = true
        
        orLabel.topAnchor.constraint(equalTo: googleButton.bottomAnchor, constant: view.frame.height/10).isActive = true
        orLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        orLabel.widthAnchor.constraint(equalTo: skipButton.widthAnchor, multiplier: 0.7).isActive = true

        nameTextField.topAnchor.constraint(equalTo: orLabel.bottomAnchor, constant: view.frame.height/10).isActive = true
        nameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: view.frame.height*0.05).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: view.frame.height/30).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: nameTextField.widthAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalTo: nameTextField.heightAnchor).isActive = true
        
        skipButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        skipButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height/20).isActive = true
        skipButton.heightAnchor.constraint(equalToConstant: view.frame.height/30).isActive = true
        skipButton.widthAnchor.constraint(equalTo: skipButton.heightAnchor, multiplier: 5/1).isActive = true
    
        warningLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        warningLabel.bottomAnchor.constraint(equalTo: skipButton.topAnchor, constant: -view.frame.height/10).isActive = true
    }
    
    func showInfo() {
//        AlternativaDAO.deleteAlternativasSelecionadas()
        self.defaults.set(true, forKey: "logado")
        present(QuestaoManager.nextQuestao()!, animated: true, completion: nil)
    }
    
    func skipLogin() {
        //AlternativaDAO.deleteAlternativasSelecionadas()
        if nameTextField.text! == "" || !isValidEmail(testStr: emailTextField.text!){
            print("Preencha os campos ou faça Login")
            shake()
            return
        } else {
            self.userInfo["image"] = UIImage(named: "anonymous")
            self.userInfo["name"] = nameTextField.text!
            self.userInfo["email"] = emailTextField.text!
            self.userInfo["photo"] = nil
            
            self.defaults.synchronize()
            
            let data : Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
            self.defaults.set(data, forKey: "userInfo")
            
            print("Info ", self.userInfo)
            
            present(QuestaoManager.nextQuestao()!, animated: true, completion: nil)
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
<<<<<<< HEAD
        self.userInfo["image"] = UIImage(named: "anonymous")
        self.userInfo["name"] = "0"
        self.userInfo["email"] = "0"
        self.userInfo["photo"] = nil
        
        self.defaults.synchronize()
        
        let data : Data = NSKeyedArchiver.archivedData(withRootObject: self.userInfo)
        self.defaults.set(data, forKey: "userInfo")
=======
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        warningLabel.isHidden = false
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
>>>>>>> 3cc821ca9c6002cae4e55e191a6c26c2aabd1b44
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(animation, forKey: "position")
    }
}
