//
//  MaisInfoViewController.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 22/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class MaisInfoViewController: TelaNormal, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    let tituloView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(r: 4, g: 59, b: 75)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let tituloLabel : UILabel = {
        let l = UILabel()
        l.text = "Ajude-nos a te conhecer melhor!"
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = UIFont(name: "Arial", size: FontManager.titulo())
        return l
    }()
    
    let escolaView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(r: 129, g: 134, b: 142)
        v.isHidden = true
        v.alpha = 0
        return v
    }()
    
    let escolaLabel : UILabel = {
        let l = UILabel()
        l.text = "Em qual instituição você estuda?"
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.alpha = 0
        l.font = UIFont(name: "Arial", size: FontManager.subtitulo())
        return l
    }()
    
    let escolaInput : UITextField = {
        let i = UITextField()
        i.font = UIFont(name: "Arial", size: FontManager.conteudo())
        i.placeholder = "Instituição"
        i.translatesAutoresizingMaskIntoConstraints = false
        i.backgroundColor = .white
        i.alpha = 0
        return i
    }()
    
    let anoView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(r: 129, g: 134, b: 142)
        return v
    }()
    
    let anoLabel : UILabel = {
        let l = UILabel()
        l.text = "Qual sua escolaridade?"
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = UIFont(name: "Arial", size: FontManager.subtitulo())
        return l
    }()
    

    let anoPicker : UIPickerView = {
        let p = UIPickerView()
        p.translatesAutoresizingMaskIntoConstraints = false
        p.backgroundColor = .white
        
        return p
    }()
    
    
    let enviarButton : UIButton = {
        let b = UIButton()
        b.setTitle("Enviar e ver meus resultados", for: .normal)
        b.titleLabel?.font = UIFont(name: "Arial", size: FontManager.conteudo())
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitleColor(.white, for: .normal)
        return b
    }()
    
    var topAnchorRef : NSLayoutConstraint?
    var heightAnchorRef : NSLayoutConstraint?
    
    var pickerData : [String] = ["Não estudo atualmente", "Ensino fundamental", "Ensino médio, nos primeiros anos", "Ensino médio, no último ano", "Ensino Superior"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TECLADO
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        escolaInput.delegate = self
        
        anoPicker.dataSource = self
        anoPicker.delegate = self
        
        view.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.backgroundColor = UIColor(r: 175, g: 180, b: 188)
        view.backgroundColor = scrollView.backgroundColor
        
        view.addSubview(scrollView)

        scrollView.addSubview(tituloView)
        tituloView.addSubview(tituloLabel)
        
        scrollView.addSubview(escolaView)
        escolaView.addSubview(escolaLabel)
        escolaView.addSubview(escolaInput)
        
        scrollView.addSubview(anoView)
        anoView.addSubview(anoLabel)
        anoView.addSubview(anoPicker)
        
        scrollView.addSubview(enviarButton)
        
        setupConstraits()
        
        enviarButton.addTarget(self, action: #selector(callFimTeste), for: .touchUpInside)

        
        view.layoutIfNeeded()
        print(scrollView.frame.height)
    }
    
    func callFimTeste(){
        let pickerNumber = anoPicker.selectedRow(inComponent: 0)
        let instituicao = escolaInput.text!
        
        let data : [String:Any] = ["escolaridade": pickerNumber, "instituicao": instituicao]
        
        let dt : Data = NSKeyedArchiver.archivedData(withRootObject: data)
        defaults.set(dt, forKey: "moreInfo")
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(FimTesteViewController(), animated: false,  completion: nil)
    }
    
    func setupConstraits(){
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1)
        heightAnchorRef?.isActive = true
        
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.layoutIfNeeded()
        
        tituloView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: view.frame.height/15).isActive = true
        tituloView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        tituloView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 7/8).isActive = true
        tituloView.layoutIfNeeded()
        
        tituloLabel.widthAnchor.constraint(equalTo: tituloView.widthAnchor, multiplier: 7/8).isActive = true
        tituloLabel.centerXAnchor.constraint(equalTo: tituloView.centerXAnchor).isActive = true
        tituloLabel.centerYAnchor.constraint(equalTo: tituloView.centerYAnchor).isActive = true
        tituloLabel.layoutIfNeeded()
        tituloView.layoutIfNeeded()
        
        tituloLabel.layoutIfNeeded()
        tituloView.layoutIfNeeded()
    
        tituloView.heightAnchor.constraint(equalToConstant: tituloLabel.frame.height+20).isActive = true
        tituloView.layoutIfNeeded()
        view.layoutIfNeeded()
        
        //ESCOLA
        
        escolaView.topAnchor.constraint(equalTo: anoView.bottomAnchor, constant: 10).isActive = true
        escolaView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 7/8).isActive = true
        escolaView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        escolaView.layoutIfNeeded()
        
        escolaLabel.topAnchor.constraint(equalTo: escolaView.topAnchor, constant: 10).isActive = true
        escolaLabel.widthAnchor.constraint(equalTo: escolaView.widthAnchor, multiplier: 7/8).isActive = true
        escolaLabel.centerXAnchor.constraint(equalTo: escolaView.centerXAnchor).isActive = true
        escolaLabel.layoutIfNeeded()
        
        escolaInput.topAnchor.constraint(equalTo: escolaLabel.bottomAnchor, constant: 10).isActive = true
        escolaInput.widthAnchor.constraint(equalTo: escolaView.widthAnchor, multiplier: 7/8).isActive = true
        escolaInput.centerXAnchor.constraint(equalTo: escolaView.centerXAnchor).isActive = true
        escolaInput.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/20).isActive = true
        UITextField.leftView(field: escolaInput)
        escolaInput.layoutIfNeeded()
        scrollView.layoutIfNeeded()
        view.layoutIfNeeded()
        
        escolaView.heightAnchor.constraint(equalToConstant: escolaInput.frame.height + escolaLabel.frame.height + 30).isActive = true
        escolaView.layoutIfNeeded()
        
        //ANO
        view.layoutIfNeeded()
        anoView.topAnchor.constraint(equalTo: tituloView.bottomAnchor, constant: 10).isActive = true
        anoView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 7/8).isActive = true
        anoView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        anoView.layoutIfNeeded()
        
        anoLabel.topAnchor.constraint(equalTo: anoView.topAnchor, constant: 10).isActive = true
        anoLabel.widthAnchor.constraint(equalTo: anoView.widthAnchor, multiplier: 7/8).isActive = true
        anoLabel.centerXAnchor.constraint(equalTo: anoView.centerXAnchor).isActive = true
        anoLabel.layoutIfNeeded()
        
        anoPicker.topAnchor.constraint(equalTo: anoLabel.bottomAnchor, constant: 10).isActive = true
        anoPicker.widthAnchor.constraint(equalTo: anoView.widthAnchor, multiplier: 7/8).isActive = true
        anoPicker.centerXAnchor.constraint(equalTo: anoView.centerXAnchor).isActive = true
        anoPicker.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 2/10).isActive = true
        anoPicker.layoutIfNeeded()
        
        scrollView.layoutIfNeeded()
        
        anoView.heightAnchor.constraint(equalToConstant: anoPicker.frame.height + anoLabel.frame.height + 30).isActive = true
        anoView.layoutIfNeeded()
        scrollView.layoutIfNeeded()
        
        //BOTAO PRA ENVIAR
        
        topAnchorRef = enviarButton.topAnchor.constraint(equalTo: anoView.bottomAnchor, constant: 10)
        topAnchorRef?.isActive = true
        
        enviarButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 7/8).isActive = true
        enviarButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        enviarButton.layoutIfNeeded()

        scrollView.contentSize.height = enviarButton.frame.maxY+10
        
        view.layoutIfNeeded()
        
    }
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    //É CHAMADA TODA VEZ QUE O PICKET TROCA DE OPÇÃO
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(row != 0){
            self.escolaView.isHidden = false
            //TROCANDO AS CONSTRAINTS
            enviarButton.removeConstraint(topAnchorRef!)
            topAnchorRef?.isActive = false
            topAnchorRef = enviarButton.topAnchor.constraint(equalTo: self.escolaView.bottomAnchor, constant: 10)
            topAnchorRef?.isActive = true
            
            //ANIMANDO
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.enviarButton.frame.origin.y = self.escolaView.frame.maxY + 10
                
            }, completion: { finished in
                self.enviarButton.layoutIfNeeded()
            })
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.escolaView.alpha = 1
                self.escolaInput.alpha = 1
                self.escolaLabel.alpha = 1
                
            })
            
        }else{
            //TROCANDO AS CONSTRAINTS
            
            topAnchorRef?.isActive = false
            topAnchorRef = enviarButton.topAnchor.constraint(equalTo: self.anoView.bottomAnchor, constant: 10)
            topAnchorRef?.isActive = true
            //ANIMANDO
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.enviarButton.frame.origin.y = self.anoView.frame.maxY + 10
                
            }, completion: { finished in
                self.enviarButton.layoutIfNeeded()
            })
            
            UIView.animate(withDuration: 0.5, animations: {
                self.enviarButton.updateConstraints()
                self.escolaView.alpha = 0
                self.escolaInput.alpha = 0
                self.escolaLabel.alpha = 0
               
            }, completion:{ (finished) in
                self.escolaView.isHidden = true
            })
        }
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let v = UIView()
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        v.backgroundColor = .white
        v.addSubview(label)
        label.font = UIFont(name: "Arial", size: FontManager.conteudo())
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalTo: v.widthAnchor, multiplier: 7/8).isActive = true
        label.centerXAnchor.constraint(equalTo: v.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: self.view.frame.height*1/30).isActive = true
        label.layoutIfNeeded()
        
        label.backgroundColor = .white
        label.textColor = .black
        label.textAlignment = .center
        
        label.text = pickerData[row]
        
        return v
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return view.frame.height*1/20
    }
    
    
    //QUANDO O TECLADO É MOSTRADO
    func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        heightAnchorRef?.isActive = false
        scrollView.removeConstraint(heightAnchorRef!)
        
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, constant: -keyboardFrame.height)
        heightAnchorRef?.isActive = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            //self.scrollView.frame.origin.y = self.enviarButton.frame.minY - keyboardFrame.height
            
        }, completion: { finished in
            self.enviarButton.layoutIfNeeded()
            
            self.scrollView.contentSize.height = self.enviarButton.frame.maxY+10
            self.scrollView.setContentOffset(CGPoint(x: 0.0, y: keyboardFrame.height-(self.view.frame.maxY-self.enviarButton.frame.maxY)+20), animated: true)
        })
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        heightAnchorRef?.isActive = false
        scrollView.removeConstraint(heightAnchorRef!)
        
        heightAnchorRef = scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, constant: 0)
        heightAnchorRef?.isActive = true
        scrollView.layoutIfNeeded()

        
        scrollView.contentSize.height = enviarButton.frame.maxY+10
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
    


