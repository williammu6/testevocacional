//
//  Protocols.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 03/02/17.
//  Copyright © 2017 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import UIKit

protocol TelaNormalProtocol {
    var scrollView : UIScrollView {get}

}

class TelaNormal : UIViewController, TelaNormalProtocol{
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = UIColor.backgroundColor()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
}
