//
//  QuestaoDAO.swift
//  TesteVocacionalDatabase
//
//  Created by Unochapeco unochapeco on 18/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class QuestaoDAO{
    class func criarQuestao(enunciado : String, categoria: String) -> Questao{
        let context = getContext() //PEGANDO O CONTEXT ATUAL
    
        let cat = CategoriaQuestaoDAO.getCategoria(byName: categoria)
        
        let questao = Questao(context: context)
        //SETANDO VARIÁVEIS
        questao.enunciado = enunciado
        questao.questao_categoriaQuestao = cat
        questao.date = NSDate()
        
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        return questao
    }
    
    
    class func getQuestoes() -> [Questao]{
        let fetchRequest: NSFetchRequest<Questao> = Questao.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        var resultados : [Questao] = []
        do{
            resultados = try getContext().fetch(fetchRequest) as [Questao]
        
        } catch{
            print("error")
        }
        return resultados
    }
    
    class func getQuestoes(byCategorias: [String]) -> [Questao]{
        let fetchRequest: NSFetchRequest<Questao> = Questao.fetchRequest()
        
        var str = ""
        
        for c in byCategorias{
            str+="questao_categoriaQuestao.nome == '\(c)' OR "
        }
        
        str+="1!=1"
        
        fetchRequest.predicate = NSPredicate(format: str)
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        var resultados : [Questao] = []
        do{
            resultados = try getContext().fetch(fetchRequest) as [Questao]
            
        } catch{
            print("error")
        }
        
        return resultados
    }
    
    
    class func deleteQuestoes() {
        let fetchRequest: NSFetchRequest<Questao> = Questao.fetchRequest()
        
        do {
            let searchResults = try getContext().fetch(fetchRequest)
            
            for u in searchResults as [NSManagedObject] {
                getContext().delete(u)
            }
        } catch {
            print("Error with request: \(error)")
        }
    }
    
}
