//
//  QuestaoManager.swift
//
//
//  Created by Unochapeco unochapeco on 05/12/16.
//
//

import Foundation

import UIKit
import CoreData

class QuestaoManager {
    
    static let sharedInstance = QuestaoManager()
    
    static var questoes : [Questao] = []
    
    static var curiosidades : [Curiosidade] = []
    
    static var curiosidadeDeveAparecer = true
    
    static func previousQuestao() -> QuestaoViewController?{
        let defaults = UserDefaults()
        
        let questionViewController = QuestaoViewController()
        
        var nQuestao = defaults.integer(forKey: "questao")
        
        print("Questao N: ", nQuestao)
        
        if nQuestao == 0 {
            print("Sem questão anterior !!!")
            return nil
        } else {
            
            nQuestao -= 1
            
            AlternativaDAO.deleteRespostaQuestao(questao: questoes[nQuestao])
        }
        
        defaults.set(nQuestao, forKey: "questao")
        
        defaults.synchronize()
        
        questionViewController.questao = questoes[nQuestao]
        
        return questionViewController
    }
    
    static func nextQuestao() -> UIViewController?{
        let questionViewController = QuestaoViewController()
        
        let defaults = UserDefaults()
        
        let nQuestao = defaults.integer(forKey: "questao")
        
        if nQuestao >= questoes.count{
            
            if defaults.integer(forKey: "etapa") == 1{ //ACABOU A ETAPA
                let fimetapa = FimEtapaViewController()
                return fimetapa
            }
            else { //ACABOU O TESTE INTEIRO
                
                //let fimteste = FimTesteViewController()
                let fimteste = MaisInfoViewController()
                
                return fimteste
            }
        }
        let numCuriosidade = defaults.integer(forKey: "curiosidade")
        if(nQuestao % 4 == 0 && nQuestao != 0 && curiosidades.count > numCuriosidade && curiosidadeDeveAparecer){
            let curiosidade = CuriosidadeViewController()
            
            curiosidade.curiosidade = curiosidades[numCuriosidade]
            curiosidadeDeveAparecer = false
            
            return curiosidade
        }
        curiosidadeDeveAparecer = true
        questionViewController.questao = questoes[nQuestao]
        defaults.set(nQuestao, forKey: "questao")
        defaults.synchronize()
        return questionViewController
    }
    
    
    }
