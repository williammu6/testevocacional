
//
//  Saude.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class Saude: SuperQuestao {
    
    class func criarQuestoes() {
        let questao1 = newQuestao(enunciado: "Você é saudável?", categoria: "Saúde")
        let aq1 = [
            newAlternativa(texto: "Sim", areas: [:], carac: ["versatilidade": true]),
            newAlternativa(texto: "Não", areas: [:], carac: ["versatilidade": true])
        ]
        assign(alternativas: aq1, questao: questao1)
    }
}
