//
//  CriarQuestoes.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 08/12/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

public let defaults = UserDefaults()


class Setup {
    class func All() {
        //CRIANDO TUDO
        
        NewCategoriaQuestao.novaCategoria()
        UsuarioDAO.criarUsuario(nome: "Renato", email: "lul@lol.com")
        NewCarac.criarCarac()
        NewArea.criarAreas()
        NewCurso.criarCursos()
        NewCuriosidade.criarCuriosidades()
        
        Genericas.criarQuestoes()
        Exatas.criarQuestoes()
        Humanas.criarQuestoes()
        Sociais.criarQuestoes()
        Ambientais.criarQuestoes()
        Saude.criarQuestoes()
        
        defaults.set(true, forKey: "cadastrado")
    }
}
