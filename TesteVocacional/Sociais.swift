//
//  Sociais.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/16/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class Sociais: SuperQuestao {

    class func criarQuestoes() {
        let questao1 = newQuestao(enunciado: "Você é socializável?", categoria: "Sociais")
        let aq1 = [
            newAlternativa(texto: "Sim", areas: [:], carac: ["trabalhar_sentado": true, "social": true, "criatividade":true , "versatilidade":true]),
            newAlternativa(texto: "Não", areas: [:], carac: ["trabalhar_sentado": true, "social": true, "criatividade":true, "versatilidade":true])
        ]
        assign(alternativas: aq1, questao: questao1)
    }
}
