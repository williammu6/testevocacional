//
//  QuestaoClass.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/19/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation

class SuperQuestao {
    
    class public func newQuestao(enunciado: String, categoria: String) -> Questao {
        let q = QuestaoDAO.criarQuestao(enunciado: enunciado, categoria: categoria)
        return q
    }
    
    class public func newAlternativa(texto: String, areas: [String:Int]?, carac: [String:Bool]?) -> Alternativa {
        let a = AlternativaDAO.criarAlternativa(texto: texto, areas: areas!, carac: carac!)
        return a
    }
    
    class public func assign(alternativas: [Alternativa], questao: Questao) {
        AlternativaDAO.assignToQuestion(alternativas: alternativas, questao: questao)
    }
}
