//
//  TestesViewController.swift
//  TesteVocacional
//
//  Created by Unochapeco unochapeco on 02/02/17.
//  Copyright © 2017 Unochapeco unochapeco. All rights reserved.
//

import UIKit

class TestesViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let barra = BarraView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(barra)
        view.backgroundColor = UIColor(r: 200, g: 200, b: 200)
        barra.voltarBloqueado = true
        barra.setup(view: self.view)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: UIViewControllerTransitioningDelegate protocol methods
    
    // return the animataor when presenting a viewcontroller
    // remmeber that an animator (or animation controller) is any object that aheres to the UIViewControllerAnimatedTransitioning protocol
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let a = TransitionManager()
        a.accessibilityFrame = view.frame
        return a
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let a = TransitionManager()
        a.accessibilityFrame = view.frame
        return a
    }

}
