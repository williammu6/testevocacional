//
//  TransitionManager.swift
//  Transition
//

import UIKit

class TransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate{
    
    // animate a change from one viewcontroller to another
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // get reference to our fromView, toView and the container view that we should perform the transition in
        
        let container = transitionContext.containerView
        let fromView = (transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? TelaNormal) //transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView = (transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? TelaNormal) //let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        
        print(fromView, toView)
        
        // set up from 2D transforms that we'll use in the animation
        let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -container.frame.width, y: 0)
        let screen = CGAffineTransform(translationX: 0, y: 50)
        
        // start the toView to the right of the screen
        toView?.scrollView.transform = offScreenRight
        
        // add the both views to our view controller
        container.addSubview(toView!.view)
        container.addSubview(fromView!.view)
        
        // get the duration of the animation
        // DON'T just type '0.5s' -- the reason why won't make sense until the next post
        // but for now it's important to just follow this approach
        let duration = self.transitionDuration(using: transitionContext)
        
        // perform the animation!
        // for this example, just slid both fromView and toView to the left at the same time
        // meaning fromView is pushed off the screen and toView slides into view
        // we also use the block animation usingSpringWithDamping for a little bounce
        fromView?.view.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0)
        UIView.animate(withDuration: duration, animations: {
            fromView!.scrollView.transform = offScreenLeft
            toView!.scrollView.transform = CGAffineTransform.identity
            toView!.view.transform = CGAffineTransform.identity
        }, completion: { finished in
            transitionContext.completeTransition(true)
            fromView!.view.transform = offScreenLeft
            print("completei")
            
        })
        
//        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .curveLinear, animations: {
//            
//            fromView?.view.transform = offScreenLeft
//            toView.transform = CGAffineTransform.identity
//            
//        }, completion: { finished in
//            
//            // tell our transitionContext object that we've finished animating
//            transitionContext.completeTransition(true)
//            
//        })
        
    }
    
    // return how many seconds the transiton animation will take
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let a = TransitionManager()
        return a
    }
    
    
}
