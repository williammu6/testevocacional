//
//  UserInfoDAO.swift
//  TesteVocacional
//
//  Created by uno2540 on 12/29/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class UserInfoDAO {
    class func criarUserInfo(nome : String, email: String, cursos: [String: Double], escolaridade: Int, instituicao: String){
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let userInfo = UserInfo(context: context)
        
        let dic: [String: Any] = [
            "nome": nome,
            "email": email,
            "escolaridade": escolaridade,
            "instituicao": instituicao,
            "cursos": cursos
        ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dic as NSDictionary, options: [])
            if let str = String(data: data, encoding: String.Encoding.utf8){
                userInfo.json = str
                print("data: ", str)
            }
        } catch {
            print("error")
        }
        
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getUserInfo() -> [UserInfo] {
        let fetchRequest: NSFetchRequest<UserInfo> = UserInfo.fetchRequest()
        
        do{
            let resultados = try getContext().fetch(fetchRequest)
            
            var arrayJson: [UserInfo] = []
            
            for u in resultados as [UserInfo]{
                arrayJson.append(u)
            }
            return arrayJson
        } catch{
            print("error")
        }
        return []
    }
}
