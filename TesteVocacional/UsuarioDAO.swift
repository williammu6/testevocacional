//
//  UsuarioDAO.swift
//  TesteVocacionalDatabase
//
//  Created by Unochapeco unochapeco on 18/11/16.
//  Copyright © 2016 Unochapeco unochapeco. All rights reserved.
//

import Foundation
import CoreData

class UsuarioDAO{
    
    
    class func criarUsuario(nome : String, email : String){
        
        let context = getContext() //PEGANDO O CONTEXT ATUAL
        
        let user = Usuario(context: context)
        
        //SETANDO VARIÁVEIS
        user.nome = nome
        user.email = email
        
        //SALVANDO O CONTEXT NA DATABASE
        do {
            try context.save() //Se essa linha for descomentada as informações ficarão salvas
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func getUsuario() -> Usuario?{
        let fetchRequest: NSFetchRequest<Usuario> = Usuario.fetchRequest()
        
        do{
            let resultados = try getContext().fetch(fetchRequest)
            
            for u in resultados as [Usuario]{
                return u
            }
        } catch{
            print("error")
        }
        
        return nil
        
    }
    
    class func deleteUsuario() {
        let fetchRequest: NSFetchRequest<Usuario> = Usuario.fetchRequest()
        
        do {
            let searchResults = try getContext().fetch(fetchRequest)
            
            for u in searchResults as [NSManagedObject] {
                getContext().delete(u)
            }
        } catch {
            print("Error with request: \(error)")
        }
    }
    
}

